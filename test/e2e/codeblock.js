var should = require('should');
var shouldhttp = require('should-http');
var assert = require('assert');
var request = require('supertest');
var Sails = require('sails')
	// sails;
var config = require('../../loadSailsrc')
var port = config.config.deyob.test.port;
var host = config.config.deyob.test.host;
var url = config.config.deyob.test.url;
var Promise = require("bluebird");
var beforeEachFixtures = [
	'fixtures/user',
	'fixtures/codes'
	// 'fixtures/exampleCode',
	// 'fixtures/instantWin'
];

var adminToken = "000111"


describe('CodeblockTest',function(){

	//REGISTRATION
	describe("Security",function(){
		before(function(){
			return utilsTest.clearAndLoad(beforeEachFixtures);
		});

		it("shouldn't create a codeblock directly",function(done){

			var params = {
				exported: false,
				idBegin: 2000,
				idEnd: 2009
			}
			request(url).post("codeblock").send(params)
			.end(function(err,res){
				res.should.not.have.status(200);
				done()
			})
		})
		it("shouldn't create a codeblock without admin token",function(done){

			var params = {
				exported: false,
				idBegin: 2000,
				idEnd: 2009
			}
			request(url).post("code/bulk").set('Authorization', 'bearer 000112').send(params)
			.end(function(err,res){
				res.should.have.status(401);
				done();
			})
		})
		
	})
	describe("Actions",function(){
		before(function(){
			return utilsTest.clearAndLoad(beforeEachFixtures);
		});

		it("should create a new codeblock",function(done){
			var params = {
				exported: false,
				idBegin: 2000,
				idEnd: 2009
			}
			request(url).post("code/bulk").set('Authorization', 'bearer 000111').send(params)
			.end(function(err,res){
				res.should.have.status(200);
				sails.models.codeblock.find({
					begin: [19,2000,10000000000000]
				})
				.sort("begin ASC")
				.then(function(codeblocks){
					codeblocks.length.should.equal(3);

					codeblocks[0].emptyAfter.should.equal((2000-19-1)); 
					codeblocks[0].emptyBefore.should.equal(0);
					codeblocks[0].prev.should.equal(18); 
					codeblocks[0].next.should.equal(2000);
					codeblocks[1].emptyBefore.should.equal((2000-19-1)); 
					codeblocks[1].emptyAfter.should.equal((10000000000000-2009-1)); 
					codeblocks[1].prev.should.equal(19); 
					codeblocks[1].next.should.equal((10000000000000)); 
					codeblocks[2].emptyAfter.should.equal(0);
					codeblocks[2].emptyBefore.should.equal((10000000000000-2009-1)); 
					codeblocks[2].prev.should.equal(2009); 
					codeblocks[2].next.should.equal(10000000000000); 

					done(); 
				})
				.catch(done)
			})
		})

		it("shouldn't delete an invalid codeblock",function(done)	{
			request(url).del("codeblock/809898").set('Authorization', 'bearer 000111')
			.end(function(err,res){
				res.status.should.not.equal(200);
				done();
			})
		})
		it("shouldn't delete an assigned codeblock",function(done)	{
			request(url).del("codeblock/15").set('Authorization', 'bearer 000111')
			.end(function(err,res){
				res.status.should.not.equal(200);
				done();
			})
		})
		it("should delete a non assigned codeblock and update boundary codeblocks",function(done)	{
			utilsTest.clearAndLoad(beforeEachFixtures)
			.then(function(){
				return sails.models.codeblock.findOne({begin:19});
			})
			.then(function(codeblock){
				should.exist(codeblock);
				request(url).del("codeblock/"+codeblock.id).set('Authorization', 'bearer 000111')
				.end(function(err,res){
					res.should.have.status(200);
					should.not.exist(err);
					sails.models.codeblock.find({
						begin: [18,10000000000000]
					})
					.sort("begin ASC")
					.then(function(codeblocks){
						codeblocks.length.should.equal(2);

						codeblocks[0].emptyAfter.should.equal((10000000000000-18-1)); 
						codeblocks[0].emptyBefore.should.equal(0);
						codeblocks[0].prev.should.equal(17); 
						codeblocks[0].next.should.equal(10000000000000);
						codeblocks[1].emptyAfter.should.equal(0);
						codeblocks[1].emptyBefore.should.equal((10000000000000-18-1)); 
						codeblocks[1].prev.should.equal(18); 
						codeblocks[1].next.should.equal(10000000000000); 

						done(); 
					})
					.catch(done);
				})
			})
			.catch(done);
		})
		
	})


})

