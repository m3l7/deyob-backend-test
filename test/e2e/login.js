var should = require('should');
var shouldhttp = require('should-http');
var assert = require('assert');
var request = require('supertest');
var Sails = require('sails')
  // sails;
var config = require('../../loadSailsrc')
var port = config.config.deyob.test.port;
var host = config.config.deyob.test.host;
var url = config.config.deyob.test.url;
var beforeEachFixtures = [
  'fixtures/user',
  // 'fixtures/exampleCode',
  // 'fixtures/instantWin'
];


//*****
//TESTS
//*****
console.log('*** Deyob test. host: '+host+' port: '+port+' url: '+url);

describe('Authentication',function(){

  //REGISTRATION
  describe('Registration',function(){

    before(function(){
      return utilsTest.clearAndLoad(beforeEachFixtures);
    });

    it('should register a new user and return an activationCode',function(done){
      var user = {
          "username" : "newUser",
          "email" : "new@userdeyob.com",
          "password" : "123456",
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.should.have.status(200);
        var data = JSON.parse(res.text);
        should.exist(data.activationCode);
        done();
      })
    });

    it('should fail when register a duplicate email, code 1101',function(done){
      var user = {
          "username" : 'newUser2',
          "email" : 'johnnysmithdeyob@gmail.com',
          "password" : "11"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1101);
        done();
      })
    })
    it('should fail when register a duplicate username, code 1102',function(done){
      var user = {
          "username" : 'johnsmith',
          "email" : 'a@a.com',
          "password" : "11"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1102);
        done();
      })
    })
    it('should fail when register a missing username, code 1103',function(done){
      var user = {
          "email" : 'a2@a.com',
          "password" : "11"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1103);
        done();
      })
    })
    it('should fail when register a missing email, code 1103',function(done){
      var user = {
          "username": "newUser4",
          "password" : "11"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1103);
        done();
      })
    })
    it('should fail when register a missing password, code 1103',function(done){
      var user = {
          "username": "newUser4",
          "email": "new3@a.com",
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1103);
        done();
      })
    })
    it('should fail when register an invalid email, code 1104',function(done){
      var user = {
          "username" : 'newUser3',
          "email" : 'rrrrrrr',
          "password" : "11"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1104);
        done();
      })
    })
    it('should fail when register a missing username for brand, code 1105',function(done){
      var user = {
          "email" : 'newbrand@a.com',
          "role": "brand"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1105);
        done();
      })
    })
    it('should fail when register a missing email for brand, code 1105',function(done){
      var user = {
          "username": "newbrand2",
          "role": "brand"
      }
      request(url).post('user').send(user)
      .end(function(err,res){
        res.status.should.equal(400);
        JSON.parse(res.text).errorCode.should.equal(1105);
        done();
      })
    })


  });


  //ACTIVATION
  describe('Activation',function(){

    beforeEach(function(done){
      utilsTest.clearDatabase()
      .then(function(){ return utilsTest.loadFixtures(beforeEachFixtures);})
      .then(function(){done();})
      .catch(done);
    });

    it('Should deactivate user and generate a 5 digit activation number after phone change',function(done){
      request(url).put('user/50004').set("Authorization","bearer 50004").send({phone: "348993999"})
      .end(function(err,res){
        res.status.should.equal(200);
        sails.models.user.findOne(50004)
        .then(function(user){
          should.exist(user);
          should.exist(user.activationCode);
          var activationCode = parseInt(user.activationCode);
          activationCode.should.be.type("number");
          activationCode.should.be.within(10000,99999);
          done()
        })
        .catch(done);
      })
    })
    it('Should activate a non activated user with a valid code',function(done){
      request(url).get('activate/99999').send()
      .end(function(err,res){
        res.status.should.equal(200);
        done();
      })
    })
    it('Should not activate a non activated user with an invalid code',function(done){
      request(url).get('activate/1111').send()
      .end(function(err,res){
        res.status.should.equal(403);
        done();
      })
    })
    it('Should not activate an already activated user',function(done){
      request(url).get('activate/123456').send()
      .end(function(err,res){
        res.status.should.equal(200);
        request(url).get('activate/123456').send()
        .end(function(err,res){
          res.status.should.equal(403);
          done();
        });
      })
    })

  });



  //LOGIN
  describe('Login',function(){

    before(function(done){
      utilsTest.clearDatabase()
      .then(function(){ return utilsTest.loadFixtures(beforeEachFixtures);})
      .then(function(){done();})
      .catch(done);
    });

    it('Should login with a valid user/pass for an activated user.',function(done){
      request(url).post('login?username=johnsmith&password=copter').send()
      .end(function(err,res){
        res.status.should.equal(200);
        done();
      })
    })
    it('Should login with a valid email/pass for an activated user.',function(done){
      request(url).post('login?email=johnnysmithdeyob@gmail.com&password=copter').send()
      .end(function(err,res){
        res.status.should.equal(200);
        done();
      })
    })
    it('Should return a token and a role when login',function(done){
      request(url).post('login?username=johnsmith&password=copter').send()
      .end(function(err,res){
        res.status.should.equal(200);
        var data = JSON.parse(res.text);
        should.exist(data.token);
        should.exist(data.role);
        done();
      })
    })
    it('Should return a valid token when login',function(done){
      should.fail();
      done();
    })
    it('Should not login with an invalid user/pass for an activated user.',function(done){
      request(url).post('login?username=johnsmith&password=123').send()
      .end(function(err,res){
        res.status.should.equal(401);
        done();
      })
    })
    it('Should not login with a valid user/pass for an inactivated user, code 1200',function(done){
      request(url).post('login?username=johnsmith3&password=copter').send()
      .end(function(err,res){
        res.status.should.equal(401);
        var data = JSON.parse(res.text);
        should.exist(data.errorCode);
        data.errorCode.should.equal(1200);        
        done();
      })
    })

  });

});