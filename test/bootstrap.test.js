var Sails = require('sails'),
  sails;
var config = require('../loadSailsrc');
var _ = require('lodash');

  
before(function(done) {

  var configs = {
      log: {
        level: 'warn'
      },
      connections: {
        deyobTestMysql: {
            adapter: 'sails-mysql',
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'deyob_e2e_tests'
        },
      },
      models: {
        connection: 'deyobTestMysql',
        migrate: "alter"
      },
      register:{
        activate: true,
        sendMail: false
      },
      port: config.config.deyob.test.port,
      environment: 'test',
      paths: {
        'fallbackEmailTemplateFolder': __dirname + '/node_modules/wejs-theme-default/templates/email'
      },
      deyob:{
        parse:{
          disabled: true
        }
      }
    };

  Sails.lift(configs, function(err, server) {
    sails = server;
    sails.config = _.merge(config.config,sails.config);
    if (err) {
      return done(err);
    }
    // here you can load fixtures, etc.
    done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  //wait for background task to be finished
  setTimeout(function(){
    sails.lower(done);
  },3000)



});
