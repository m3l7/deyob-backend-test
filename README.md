### Warning: stripped, not working, version of deyob-backend

# Table of contents



[TOC]

# Install

clone the repo and install dependencies:

      npm install
      bower install

# Deployment specific configuration

Fill your .sailsrc (either on the project or the home folder) with:

      {
            "deyob":{
                  "API":{
                        "store": "http://localhost:14050/apiv1/store/",
                        "qrcodeScan": "http://a.deyob.com/",
                        "qrcodeInvalidate": "http://w.deyob.com/"
                  },
                  "skebby":{
                          "username": "deyob",
                          "password": "Deyob2015@",
                          "sender_string": "Deyob"
                  },
                  "tokenExpiration": 5184000000,
                  "production": false,
                  "test":{
                        "port": 1338,
                        "host": "localhost",
                        "url": "http://localhost:1338/apiv1/"
                  },
        	    	  "parse":{
              			"appID": "Pg8KOlV5p4KLPg8ybL9SbwUF3UtVaLUVxTts4wb1",
              			"masterKey": "HNiioVzttCsbsFCbPvx4I37NOPRYq8qc4tkC3TyQ",
                    "disabled": false
            		  },
                  "registerUrl": "http://localhost:14050/apiv1/activate",
                  "email":{
                            "email": "testybacon0@gmail.com",
                            "service": "Gmail",
                            "user": "testybacon0@gmail.com",
                            "pass": "crispybacon001",
                            "sendTo": "kknull0@gmail.com"
                  },
                  "twitter":{
                        "consumerKey": "xxxx",
                        "consumerSecret": "xxx"
                  },
                  "facebook":{
                        "clientID": "xxx",
                        "clientSecret": "xxx"
                  },
                  "google":{
                        "webClientID": "xxx",
                        "androidClientID": "xxx",
                        "iosClientID": "xxx",
                        "apiKey": "xxx"
                  }
            }
      }

Set production to false to enable some debug/dev APIs



# Development

Start the server with:

      sails lift

or

      node deyob_backend.js

# Coding Conventions

#### Models
For every model change, make a migration (i.e. sql script) and put it in `migrations/migration/{date}_{description}`.

#### Code
Remember to update this README when needed (i.e. when .sailsrc must be changed)

#### Git
make separate branches for new features or bugfixing.
Merge them to a development branch or to master, and update the version, both in the package.json and as a new git tag, using the following rule:

      `x.y.z`

  - Change z when a bugfix is applied
  - Change y when a new feature is introduced
  - Change x when a descructive feature is introduced (i.e. features which need a new frontend version)

# Testing

e2e tests are located in test/.

Use npm to run the tests: 

      npm test

You can run only a specific subset of tests (i.e. Activation tests) with:

      npm test -- --grep Activation


# Deployment

Login to remote server, i.e.:

      ssh ec2-user@deyob

the backend is usually located in `projects/deyob-backend`.

#### Strategy

Check the current revision, i.e. with:

      git rev-parse HEAD

Compare the revision with the code to pull, checking for migrations and other changes

#### Database Backup

Make a database backup:

      ~/projects/scripts/db_backup.sh

Make sure that the backup was created in `projects/deyob_sql_dump/`

#### Pull

Pull the code from remote (i.e. from master):

     git pull origin master

#### Update dependencies

      npm install

#### sailsrc
Make manual changes to `~/.sailsrc`, according to the readme.

#### Migrations

Do new migrations from the `migrations/` folder, i.e.:

      mysql -u root deyob < migrations/migration_date_description.sql

Check the db if the migration were run correctly, and check the data integrity.

#### Tests

Run the tests:

      npm test

#### Restart the server:

Fetch current pm2 id with:

      pm2 list

Restart the backend (i.e. with id=1):

      pm2 restart 1

#### Logs

Check the logs with pm2:

      pm2 logs 1

The server must have started correctly.

# Debugging

#### Local debug
For small fixes, it is possible to run the backend with the node debugger attached:

      node debug deyob-backend.js

If you want to run a different instances with a different port, set it with:

      node debug deyob-backend.js --port=xxxx

#### Remote debug
It is possible to debug remotely using:

      node --debug [--port=xxxx] deyob-backend.js

Then, on a remote machine, attach to the debug instance with:

      node debug host:port

#### Remote debug with node-inspector

If you want to debug directly from a chrome, use node-inspector:

      node --debug [--port=xxxx]
      node-inspector [--web-port=yyyy]

Finally, open the provided link with chrome.

N.B: xxxx and yyyy ports must be accessible from the wan.



