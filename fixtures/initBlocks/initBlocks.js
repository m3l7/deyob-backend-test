module.exports = [
	{
		"model": "codeblock",
		"items":[
			{
				//dummy code
				"id": 1,
				"begin": 0,
				"end": 0,
				"emptyAfter": 10000000000000-1,
				"emptyBefore": 0,
				"prev": 0,
				"next": parseInt("FFFFFFFFFFF",16),
				"date": new Date(),
				"action": "",
				"exported": false,
				"deleted": true
			},
			{
				//dummy code
				"id": 2,
				"begin": 10000000000000,
				"end": 10000000000000,
				"emptyAfter": 0,
				"emptyBefore": 10000000000000-1,
				"prev": 0,
				"next": 10000000000000,
				"date": new Date(),
				"action": "",
				"exported": false,
				"deleted": true
			}
		]
	}
]