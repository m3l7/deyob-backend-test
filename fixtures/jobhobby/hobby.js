module.exports = [
	{
		"model": "hobby",
		"items":[
			{
				"id": 1,
				"name": "Running"
			},
			{
				"id": 2,
				"name": "Walking"
			},
			{
				"id": 3,
				"name": "Cycling"
			},
			{
				"id": 4,
				"name": "Indoor Sport"
			},
			{
				"id": 5,
				"name": "Outdoor Sport"
			},
			{
				"id": 6,
				"name": "Mountain"
			},
			{
				"id": 7,
				"name": "Sea"
			},
			{
				"id": 8,
				"name": "Music"
			},
			{
				"id": 9,
				"name": "Dancing"
			},
			{
				"id": 10,
				"name": "Travel"
			},
			{
				"id": 11,
				"name": "Animals"
			},
			{
				"id": 12,
				"name": "Motorsport"
			},
			{
				"id": 13,
				"name": "Golf"
			}
		]
	}
]