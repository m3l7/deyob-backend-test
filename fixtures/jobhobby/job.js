module.exports = [
	{
		"model": "job",
		"items": [
			{
				"id": 1,
				"name": "Architetto"
			},
			{
				"id": 2,
				"name": "Avvocato"
			},
			{
				"id": 3,
				"name": "Notaio"
			},
			{
				"id": 4,
				"name": "Commercialista"
			},
			{
				"id": 5,
				"name": "Ingegnere"
			},
			{
				"id": 6,
				"name": "Impiegato"
			},
			{
				"id": 7,
				"name": "Impiegato pubblico/insegnante"
			},
			{
				"id": 8,
				"name": "Medico"
			},
			{
				"id": 9,
				"name": "Operaio"
			},
			{
				"id": 10,
				"name": "Dirigente"
			},
			{
				"id": 11,
				"name": "Libero professionista"
			},
			{
				"id": 12,
				"name": "Imprenditore"
			},
			{
				"id": 13,
				"name": "Non occupato"
			},
			{
				"id": 14,
				"name": "Studente"
			},
			{
				"id": 15,
				"name": "Altro"
			}
		]
	}
]