module.exports = [
	{
		"model": "campaign",
		"items":[
			{
				"id":10000,
				"type": 1,
				"brand": 10002,
				"name":"CrispyCampaign",
				"description": "Deyob organizza una lotteria a premi con vincita istantanea ad estrazione finale denominata 'Deyob Spring', con 2.000 (duemila/00) tagliandi, sensibilizzando gli stessi cittadini a contribuire economicamente, tramite l’acquisto dei relativi biglietti.",
				"image": "campaignTest.png"
			}
		]
	},
	{
		"model": "codeblock",
		"items":[
			{
				"id": 12,
				"begin": 5,
				"end": 5,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"next": 6,
				"prev": 4,
				"date": new Date(),
				"action": true,
				"actionValue": "<iframe src='https://www.youtube.com/embed/vn37_avWQao' frameborder='0' allowfullscreen></iframe>",
				"exported": true,
				"deleted": false,
				"assigned": true,
				"points": 0,
				"type": 0,
				"brand": 10002,
				"campaign": 10000,
			}
		]
	},
	{
		"model": "code",
		"items":[
			{
				"id": 5,
				"encryptedCode": "lotit8QQqtgfnKCZFfcRy8D4Q1tcG969CnAHFtuZce4",
				"trackingCode": 5,
				"codeBlock": 12
			}
		]
	}
]