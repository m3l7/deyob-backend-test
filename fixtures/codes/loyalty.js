module.exports = [
	{
		"model": "campaign",
		"items":[
			{
				"id":41001,
				"type": 4,
				"brand": 10002,
				"name":"Campagna Caffe",
				"description": "Raccogli 10 caffe e il successivo e' in omaggio",
				"image": "caffe.jpg"
			}
		]
	},
	{
		"model": "award",
		"items":[
			{
				"id":41001,
				"campaign": 41001,
				"name":"Caffe Lavazza.",
				"description": "Caffe lavazza. Il caffe preferito dagli italiani.",
				"image": "caffe.jpg",
				"points": 4
			}
		]
	},
	{
		"model": "codeblock",
		"items":[
			{
				"id": 15,
				"begin": 9,
				"end": 17,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"next": 18,
				"prev": 8,
				"date": new Date(),
				"action": false,
				"exported": true,
				"deleted": false,
				"assigned": true,
				"type": 0,
				"brand": 10002,
				"campaign": 41001
			}
		]
	},
	{
		"model": "code",
		"items":[
			{
				"id": 9,
				"encryptedCode": "ZFq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 9,
				"codeBlock": 15
			},
			{
				"id": 10,
				"encryptedCode": "ZGq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 10,
				"codeBlock": 15
			},
			{
				"id": 11,
				"encryptedCode": "ZHq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 11,
				"codeBlock": 15
			},
			{
				"id": 12,
				"encryptedCode": "ZIq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 12,
				"codeBlock": 15
			},
			{
				"id": 13,
				"encryptedCode": "Z9q8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 13,
				"codeBlock": 15
			},
			{
				"id": 14,
				"encryptedCode": "Z8q8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 14,
				"codeBlock": 15
			},
			{
				"id": 15,
				"encryptedCode": "Z7q8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 15,
				"codeBlock": 15
			},
			{
				"id": 16,
				"encryptedCode": "Z6q8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 16,
				"codeBlock": 15
			},
			{
				"id": 17,
				"encryptedCode": "Z5q8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 17,
				"codeBlock": 15
			}
		]
	}
]
