module.exports = [
	{
		"model": "codeblock",
		"items":[
			{
				//dummy code
				"id": 1,
				"begin": 0,
				"end": 0,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"prev": 0,
				"next": 1,
				"date": new Date(),
				"action": "",
				"exported": false,
				"deleted": true
			},
			{
				//dummy code
				"id": 2,
				"begin": 10000000000000,
				"end": 10000000000000,
				"emptyAfter": 0,
				"emptyBefore": 10000000000000-20,
				"prev": 19,
				"next": 10000000000000,
				"date": new Date(),
				"action": "",
				"exported": false,
				"deleted": true
			}
		]
	}
]