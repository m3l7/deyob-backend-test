module.exports = [
	{
		"model": "product",
		"items":[
			{
				"id":41000,
				"name":"Cheerios",
				"description": "Che buona la vita!"
			},
			{
				"id":41001,
				"name":"Dash",
				"description": "Lavano, smacchiano e donano brillantezza!"
			}
		]
	},
	{
		"model": "imageproduct",
		"items":[
			{
				"id":41000,
				"path":"cheerios.jpg",
				"product": 41000
			},
		]
	},
	{
		"model": "campaign",
		"items":[
			{
				"id":41000,
				"type": 3,
				"brand": 10002,
				"name":"Campagna Nestle",
				"description": "Raccogli i punti cheerios e vinci qualcosa",
				"image": "nestle.png"
			}
		]
	},
	{
		"model": "award",
		"items":[
			{
				"id":41000,
				"campaign": 41000,
				"name":"Fiat Panda",
				"description": "Fiat Panda biturbo 160CV",
				"image": "panda.jpg",
				"points": 270
			},
			{
				"id":41002,
				"campaign": 41000,
				"name":"Kawasaki Ninja ZX-10R",
				"description": "Superbike Supremacy",
				"image": "kawasaki.jpg",
				"points": 300
			},
			{
				"id":41003,
				"campaign": 41000,
				"name":"Iphone 6",
				"description": "Mai cosi ampio, mai cosi sottile, mai cosi iPhone",
				"image": "iphone.jpg",
				"points": 50
			}
		]
	},
	{
		"model": "codeblock",
		"items":[
			{
				"id": 13,
				"begin": 6,
				"end": 6,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"next": 7,
				"prev": 5,
				"date": new Date(),
				"action": false,
				"exported": true,
				"deleted": false,
				"assigned": true,
				"points": 50,
				"type": 0,
				"brand": 10002,
				"campaign": 41000,
				"product": 41000
			},
			{
				"id": 14,
				"begin": 7,
				"end": 8,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"next": 9,
				"prev": 6,
				"date": new Date(),
				"action": false,
				"exported": true,
				"deleted": false,
				"assigned": true,
				"points": 200,
				"type": 0,
				"brand": 10002,
				"campaign": 41000,
				"product": 41001
			}
		]
	},
	{
		"model": "code",
		"items":[
			{
				"id": 60000,
				"encryptedCode": "ZEq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 60000,
				"codeBlock": 13
			},
			{
				"id": 60005,
				"encryptedCode": "ZZq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 60005,
				"codeBlock": 14
			},
			{
				"id": 60006,
				"encryptedCode": "ZWq8uyG/5RCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 60006,
				"codeBlock": 14
			}
		]
	}
]
