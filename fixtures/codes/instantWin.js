module.exports = [
	{
		"model": "imageproduct",
		"items":[
			{
				"id":40000,
				"path":"nesquik.jpg",
				"product": 40000
			},
			{
				"id":40002,
				"path":"penne_rigate.jpg",
				"product": 40001
			},
			{
				"id":40001,
				"path":"nesquik2.jpg",
				"product": 40000
			}
		]
	},
	{
		"model": "campaign",
		"items":[
			{
				"id":40000,
				"type": 2,
				"brand": 10002,
				"name":"InstantWinCampaign",
				"description": "Scopri subito se hai vinto o perso fantastici premi",
				"image": "campaignPanda.png"
			}
		]
	},
	{
		"model": "award",
		"items":[
			{
				"id":40000,
				"campaign": 40000,
				"name":"Fiat Panda",
				"description": "Fiat Panda biturbo 160CV",
				"image": "panda.jpg"
			}
		]
	},
	{
		"model": "product",
		"items":[
			{
				"id":40000,
				"name":"Nesquik",
				"description": "La colazione diventa un gioco!",
				"points": 30
			}
		]
	},
	{
		"model": "codeblock",
		"items":[
			{
				"id": 11,
				"begin": 1,
				"end": 4,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"next": 5,
				"prev": 0,
				"date": new Date(),
				"action": true,
				"actionValue": "<p><b>Bravo!</b></p><p>Hai appena scansionato un codice deyob!</p><img src='ok.png'>",
				"exported": true,
				"deleted": false,
				"assigned": true,
				"points": 0,
				"type": 0,
				"brand": 10002,
				"campaign": 40000,
				"product": 40000
			}
		]
	},
	{
		"model": "code",
		"items":[
			{
				"id": 1,
				"encryptedCode": "nG5it8QQqtgfnKCZFfcRy8D4Q1tcG969CnAHFtuZce4",
				"trackingCode": 1,
				"winner": 1,
				"codeBlock": 11
			},
			{
				"id": 2,
				"encryptedCode": "uqXWVaCg2JYGPwZ4o+ACoOmKdlkQdgi+xAFP+HtkB7c",
				"trackingCode": 2,
				"winner": 1,
				"codeBlock": 11
			},
			{
				"id": 3,
				"encryptedCode": "7y88yJRiuujxV3D+0EIhBQg04/vUIOhfKDP5CYcKYkA",
				"trackingCode": 3,
				"winner": 1,
				"codeBlock": 11
			},
			{
				"id": 4,
				"encryptedCode": "svx6m8ULc5E3A08dhx8qMvclrEpWFiTcWy/HRb5v1Dw",
				"trackingCode": 4,
				"winner": 0,
				"codeBlock": 11
			}
		]
	}
]