module.exports = [
	{
		"model": "campaign",
		"items":[
			{
				"id": 70000,
				"name": "campagna virale deyob",
				"type": 5,
				"brand": 10002
			}
		]
	},
	{
		"model": "codeblock", "items":[
			{
				"id": 16,
				"begin": 18,
				"end": 18,
				"emptyAfter": 0,
				"emptyBefore": 0,
				"next": 19,
				"prev": 17,
				"date": new Date(),
				"action": false,
				"exported": true,
				"deleted": false,
				"assigned": true,
				"type": 1,
				"points": 55,
				"brand": 10002,
				"campaign": 70000
			},
			{
				"id": 17,
				"begin": 19,
				"end": 19,
				"emptyAfter": 10000000000000-20,
				"emptyBefore": 0,
				"next": 10000000000000,
				"prev": 18,
				"date": new Date(),
				"action": false,
				"exported": true,
				"deleted": false,
				"assigned": false,
				"type": 2,
				"points": 99,
				"brand": 10002,
				"campaign": 70000
			}
		]
	},
	{
		"model": "code",
		"items":[
			{
				"id": 18,
				"encryptedCode": "viralmothRCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 18,
				"codeBlock": 16
			},
			{
				"id": 19,
				"encryptedCode": "viralspotRCRY7LvhEXYubYRGjg5N6KDCxqcMbtMKcE",
				"trackingCode": 19,
				"codeBlock": 17
			}
		]
	}
]
