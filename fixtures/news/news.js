module.exports = [
	{
		"model": "news",
		"items":[
			{
				"id": 10000,
				"title": "Il sistema integrato Deyob",
				"description": "Il sistema DEYOB rappresenta uno strumento FACILE ed economico per la TUTELA dei brand di prodotto e la loro tracciabilità.\nÈ in grado di garantire l’autenticità del singolo prodotto, di tracciarne la filiera, di mettere in relazione tra loro i tre attori principali: produttore, trade, consumatore. La sua modularità ne fa un servizio adattabile tanto alla piccola quanto alla media e grande impresa, in grado di attivare relazioni efficaci tra produttore, retail, consumatore."
			}
		]
	},
	{
		"model": "news",
		"items":[
			{
				"id": 10001,
				"title": "Nuove campagne Deyob",
				"description": "Il sistema DEYOB rappresenta uno strumento FACILE ed economico per la TUTELA dei brand di prodotto e la loro tracciabilità.\nÈ in grado di garantire l’autenticità del singolo prodotto, di tracciarne la filiera, di mettere in relazione tra loro i tre attori principali: produttore, trade, consumatore. La sua modularità ne fa un servizio adattabile tanto alla piccola quanto alla media e grande impresa, in grado di attivare relazioni efficaci tra produttore, retail, consumatore."
			}
		]
	},
	{
		"model": "imagenews",
		"items":[
			{
				"id": 10000,
				"path": "news1.jpg",
				"news": 10000
			},
			{
				"id": 10002,
				"path": "news1.jpg",
				"news": 10001
			},
			{
				"id": 10001,
				"path": "news2.png",
				"news": 10000
			}
		]
	}
];