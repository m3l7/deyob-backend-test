// var date = new Date();
// date.setHours(date.getHours()-120);
// dates.push(date);


var mod = [
	{
		"model": "user",
		"items":[
			{
				"id": 30000,
				"gender": "male",
				"age": 24,
				"email":"test31@deyob.com",
				"username":"test31",
				"role":"user"
			},
			{
				"id": 30001,
				"gender": "female",
				"age": 47,
				"email":"test30@deyob.com",
				"username":"test32",
				"role":"user"
			}
		]
	},
	{
		"model":"user",
		"items":[
			{	
				"id": 30002,
				"name": "CrispyBrandBulk",
				"location": "Marostica",
				"link": "http://deyob.com",
				"phone": "0444323322",
				"email":"test33@deyob.com",
				"username":"test33",
				"role":"brand"
			}
		]
	},
	{
		"model": "campaign",
		"items":[
			{
				"id":30000,
				"type": 1,
				"typeName":"lotteria",
				"brand": 30002,
				"name":"CrispyCampaignBulk",
				"description": "Deyob organizza una lotteria a premi con vincita istantanea ad estrazione finale denominata 'Deyob Spring', con 2.000 (duemila/00) tagliandi, sensibilizzando gli stessi cittadini a contribuire economicamente, tramite l’acquisto dei relativi biglietti.",
				"image": "campaignTest.png"
			},
			{
				"id":30001,
				"type": 1,
				"typeName":"lotteria",
				"brand": 30002,
				"name":"CrispyCampaignBulk2",
				"description": "Deyob organizza una lotteria a premi con vincita istantanea ad estrazione finale denominata 'Deyob Spring', con 2.000 (duemila/00) tagliandi, sensibilizzando gli stessi cittadini a contribuire economicamente, tramite l’acquisto dei relativi biglietti.",
				"image": "campaignTest.png"
			}
		]
	},
	{
		"model": "scanlog",
		"items":[]
	}
];



for (i=0;i<1000;i++){
	var model = {
				"id": 30001+i,
				"os": "ios",
				"scanDate": new Date(),
				"campaign": 10000,
				"user": 10001,
				"lat": "45.7657290",
				"lng": "11.7272750",
				"country":"Italia",
				"countryCode":"it",
				"location":"Milano",
				"brand": 10002,
				"newUser": false,
				"user_age": 24,
				"user_gender": "female",
				"user_username": "lol",
				"code": 1000
			};
	model.scanDate.setHours(model.scanDate.getHours()-Math.floor(Math.random()*100));

	mod[3].items.push(model);

}

module.exports = mod;