/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * Only applies to HTTP requests (not WebSockets)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.http.html
 */

var chalk = require('chalk');
require("date-format-lite");
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');
var fields = ["trackingCode", "idBegin","idEnd","prev","next","begin","end","emptyAfter","emptyBefore"];

module.exports.http = {

  /****************************************************************************
  *                                                                           *
  * Express middleware to use for every Sails request. To add custom          *
  * middleware to the mix, add a function to the middleware config object and *
  * add its key to the "order" array. The $custom key is reserved for         *
  * backwards-compatibility with Sails v0.9.x apps that use the               *
  * `customMiddleware` config option.                                         *
  *                                                                           *
  ****************************************************************************/

  customMiddleware: function (app) {
      var swagger = require('sails-swagr');  
      var express = require('sails/node_modules/express');

      app.use(swagger.init(express, app, {
          apiVersion: '1.0',
          swaggerVersion: '2.0',
          swaggerURL: '/api/docs',
          swaggerJSON: '/api-docs.json',
          basePath: sails.getBaseurl(),
          info: {
            title: ' API Swagger Documentation',
            description: 'Sails Swagr'
          },
          apis: [
            // './api/docs/Cards.yml', 
            // './api/docs/Stories.yml',
            // './api/docs/Users.yml',
          ]
      })); 
      sails.on('ready', function() {
        swagger.sailsGenerate({
          routes: sails.router._privateRouter.routes,
          models: sails.models
        });
      });
    },



  middleware: {

    passportInit    : require('passport').initialize(),
    passportBearer    : require('passport').initialize(),
    rawBody: function(req,res,next){
      req.rawBody = '';
      req.setEncoding('utf8');

      req.on('data', function(chunk) { 
        req.rawBody += chunk;
      });

      req.on('end', function() {
        next();
      });
    },
    parseTrackingCode: function(req,res,next){

      fields.forEach(function(field){
        if ((!!req.body) && (!!req.body[field])) req.body[field] = utilsCode.toInt(req.body[field]);
        if ((!!req.query) && (!!req.query[field])) req.query[field] = utilsCode.toInt(req.query[field]);

      })

      if ((!!req.query) && (!!req.query.where)) req.query.where = parseWhere(req.query.where);
      if ((!!req.body) && (!!req.body.where)) req.body.where = parseWhere(req.body.where);

      next()

    },
    // prerender: require('prerender-node').set('prerenderToken','nbaYh86ntPEglSs4PArW'),

  /***************************************************************************
  *                                                                          *
  * The order in which middleware should be run for HTTP request. (the Sails *
  * router is invoked by the "router" middleware below.)                     *
  *                                                                          *
  ***************************************************************************/

    order: [
      // "$custom",
      "responseTimeLogger",
      'startRequestTimer',
      // 'cookieParser',
      // 'session',
      // 'myRequestLogger',
      // 'prerender',  //for SEO bot crawlers
      // 'rawBody',
      'passportInit',
      'requestLogger',
      'bodyParser',
      'handleBodyParserError',
      'parseTrackingCode',
      'compress',
      'methodOverride',
      // 'poweredBy',
      // '$custom',
      'router',
      'www',
      'favicon',
      '404',
      '500',
    ],

    requestLogger: function(req, res, next) {
        var now = new Date();
        sails.log.info(now.format("iso")+ " - " + chalk.cyan(req.method) + ' ' + req.url);    
        // if (req.query) console.log(JSON.stringify(req.query));
        if (req.body) sails.log.info(JSON.stringify(req.body));
        return next();
    },  
    responseTimeLogger: function (req, res, next) {
          res.on("header", function() {
            var now = new Date();
            sails.log.info(now.format("iso") + " - " + "RESPONSE: "+res.statusCode+" "+res.getHeader('X-Response-Time'));
          });
          require('response-time')()(req, res, next);
    }
  /****************************************************************************
  *                                                                           *
  * Example custom middleware; logs each request to the console.              *
  *                                                                           *
  ****************************************************************************/

    // myRequestLogger: function (req, res, next) {
    //     console.log("Requested :: ", req.method, req.url);
    //     return next();
    // }


  /***************************************************************************
  *                                                                          *
  * The body parser that will handle incoming multipart HTTP requests. By    *
  * default as of v0.10, Sails uses                                          *
  * [skipper](http://github.com/balderdashy/skipper). See                    *
  * http://www.senchalabs.org/connect/multipart.html for other options.      *
  *                                                                          *
  ***************************************************************************/

    // bodyParser: require('skipper')

  },

  /***************************************************************************
  *                                                                          *
  * The number of seconds to cache flat files on disk being served by        *
  * Express static middleware (by default, these files are in `.tmp/public`) *
  *                                                                          *
  * The HTTP static cache is only active in a 'production' environment,      *
  * since that's the only time Express will cache flat-files.                *
  *                                                                          *
  ***************************************************************************/

  // cache: 31557600000
};
function parseWhere(where){
    where = JSON.parse(where);
    for (var i in where){
      if (fields.indexOf(i)!=-1){
        if (typeof where[i] != "object") where[i] = utilsCode.toInt(where[i]);
        else for (var j in where[i]){
          where[i][j] = utilsCode.toInt(where[i][j]);
        }
      }
    }

    return JSON.stringify(where);
}