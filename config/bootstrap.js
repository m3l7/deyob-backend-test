/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */
var fixtures = require('sails-fixtures');
var path = require("path")

module.exports.bootstrap = function(cb) {

	if (process.env.NODE_ENV=='development'){

		var fixturesPath = null;
		process.argv.forEach(function(arg){
			if (arg.indexOf('--fixtures')==0) fixturesPath = arg.split('--fixtures=')[1];
		})
		if (fixturesPath){
			console.log('loading fixtures')
			fixtures.loadFromFiles({
			    'dir':fixturesPath,
			    'pattern':'*.*' // Default is '*.json' 
			}, function(err,files){
				if (err) sails.log.error("Error: "+err);
				else sails.log("Loaded "+files+" files");
				process.exit();
			});
		}
		else cb();
	}
	else cb();

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
};
