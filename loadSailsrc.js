var fs = require('fs');
var jf = require('jsonfile')
var home = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE || '';

var config = {};
if (existSync('./.sailsrc')) config = jf.readFileSync('./.sailsrc');
else if (existSync(home+'/.sailsrc')) config = jf.readFileSync(home+'/.sailsrc');

module.exports = {
	config: config
}

function existSync(filePath){
	try{
		fs.statSync(filePath);
	}catch(err){
		if(err.code == 'ENOENT') return false;
	}
	return true;
}