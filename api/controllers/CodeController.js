/**
 * CodeController
 *
 * @description :: Server-side logic for managing codes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var json2csv = require('json2csv');
var fs = require('fs');
var path = require('path');
var q = require('q');
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');
var _ = require("lodash");
  
module.exports = {

	scan: function(req,res){
		//TODO: CREATE SCANLOG TABLE (DIRECTLY IN REDIS?)
		q().then(function(){
			var base64encrypt = req.body.code;
			if (!base64encrypt) return q.reject(errorCodes[1001]);
			else return sails.models.code.findOne({encryptedCode:base64encrypt});
		})
		.then(function(code){
			if (!code) return q.reject(errorCodes[1001]);
			else return utilsCode.populateCode(code,req);
		})
		.then(utilsCode.checkCodeRelationsValidity) //check if product and campaign are presents and expire logic
		.then(function(code){
			return utilsCode.checkCodeValidity(code,req);
		})
		.then(utilsCode.updateUserProductAfterScan)
		.then(utilsCode.updateCampaignAfterScan)
		.then(utilsCode.doCampaignsLogic)	//do campaign logic (instant win, loyalty...)
		.then(utilsCode.generateInvalidateCodes) //find awards and fill invalidate qrcodes
		.then(utilsCode.updateCampaignInfo)
		.then(function(code){
			// SEND CODE
			sails.log.silly("Sending Code to the client");
			var codeObj = _.cloneDeep(code);
			res.send(code);
			return codeObj;
		})
		.then(function(code){
			//LAZY OPERATIONS, AT THIS POINT WE HAVE ALREADY SENT A RESPONSE TO THE CLIENT.
			return utilsCode.updateScanLog(code,req);
		})
		.then(function(code){
			return utilsCode.updateScanStatus(code,req);
		})
		.then(utilsCode.updateCampaignAfterScan)
		.then(utilsCode.updateCodeGeo)
		.catch(res.customError);
	},
	bulkGen: function(req,res){

		//GENERATE CODES
		var values = actionUtil.parseValues(req);
		var idBegin = values.idBegin;
		var idEnd = values.idEnd;
		var range = idEnd-idBegin;
		if (values.exported=="true") values.exported = true;
		if (values.action=="true") values.action = true;

		if ((!values.idBegin) || (!values.idEnd)) res.badRequest({summary:'ID range mancante',errorCode:5000},{forceOutput:true});
		else {
			delete values.idBegin;
			delete values.idEnd;

			utilsUser.lockUser(req.user) //avoid operations flooding
			.then(function(){
				return utilsCode.isRangeValid(idBegin,idEnd);
			})
			.then(function(valid){
				//upload image
				if (!valid) return q.reject({status: 400, summary: "Range non valido",errorCode: 5001});
				else return utilsUpload.uploadImageToStore(req,'actions/');
			})
			.then(function(filepath){
				values.action = (filepath) ?  '<img src="'+filepath+'">' : "";
				return q.all([
					sails.models.codeblock.find({where:{
						end:{"<=": idBegin}
					},limit:1,sort: "end DESC"}),
					sails.models.codeblock.find({where:{
						begin:{">=": idEnd}
					},limit:1,sort: "begin ASC"})
				]);
			})
			.spread(function(leftCode,rightCode){
				//create codeblock and update near codeblocks
				if ((!leftCode) || (!rightCode)) throw new Error("Impossibile trovare blocchi contigui.");
				else{
					var leftCode = leftCode[0];
					var rightCode = rightCode[0];
					var emptyBefore = idBegin-leftCode.end-1;
					var emptyAfter = rightCode.begin-idEnd-1;

					return q.all([
						sails.models.codeblock.create({
							begin:idBegin,
							end:idEnd,
							emptyBefore:emptyBefore,
							emptyAfter:emptyAfter,
							prev:leftCode.end,
							next:rightCode.begin,
							date: new Date(),
							action: values.action,
							exported: values.exported,
							campaign: values.campaign,
							winner: values.winner,
							brand: values.brand,
							deleted: false,
							points: 0,
							type: 0,
							brand: values.brand,
							campaign: values.campaign,
							product: values.product
						}),
						sails.models.codeblock.update({id:leftCode.id},{emptyAfter:emptyBefore,next: idBegin}),
						sails.models.codeblock.update({id:rightCode.id},{emptyBefore:emptyAfter,prev: idEnd})
					]);
				}
			})
			.spread(function(codeBlock,left,right){
				if ((!codeBlock) || (!left) || (!right)) return q.reject("Impossibile creare il codeblock o aggiornare i codeblock esistenti");

				//SEND OK TO CLIENT -- GENERATION OF CODES ARE DONE IN BACKGROUND AND SENT BY MAIL
				res.send();
				return utilsCode.createCodes(idBegin,idEnd,1000,codeBlock.id);
			})
			.catch(function(err){
				res.customError(err);
				//unlock user
				if ((err) && (err.errorCode!=6001)) utilsUser.unlockUser(req.user);
			})
			.then(function(){
				return utilsMail.sendCodesGeneratedMail(req.user.email,idBegin,idEnd);
			})
			.then(function(){
				utilsUser.unlockUser(req.user);
			})
			.catch(function(err){
				sails.log.error(err);
				utilsUser.unlockUser(req.user);
			})
		}
	},
	bulkEdit: function(req,res){
		var values = actionUtil.parseValues(req);
		if ((!values.idBegin) || (!values.range)) res.badRequest({summary:'range mancante',errorCode:5000},{forceOutput:true});
		else{
			var idBegin = parseInt(values.idBegin);
			var range = parseInt(values.range);
			var idEnd = idBegin + range -1;


			delete values.idBegin;
			delete values.range;

			utilsCode.editCodes(idBegin,idEnd,values,req)
			.then(function(){
				res.send();
			})
			.catch(res.customError);
		}
	},
	export: function(req,res){
		if ((!req.params.idBegin) || (!req.params.idEnd)) res.badRequest({summary: "idBegin o idEnd mancanti",errorCode:1400},{forceOutput:true})
		else {
			var idBegin = utilsCode.toInt(req.params.idBegin);
			var idEnd = utilsCode.toInt(req.params.idEnd);
			var values = actionUtil.parseValues(req);

			q()
			.then(function(){
				return sails.models.codeblock.findOne({
					begin: idBegin,
					end: idEnd
				})
				.then(function(block){
					if (!block) return q.reject(errorCodes[5001]);
				})
			})
			.then(function(){
				return sails.models.code.find({
					where:{
						trackingCode:{
							'>=': idBegin,
							'<=': idEnd
						},
					},
					sort: "trackingCode ASC"
				});				
			})
			.then(function(codes){
				var codesArr = [];
				codes.forEach(function(code){
					codesArr.push({
						trackingCode: code.trackingCode,
						// trackingCode: utilsCode.toCode(code.trackingCode),
						encryptedCode: "http://a.deyob.com/"+code.encryptedCode
					});
				})

				return q.promise(function(resolve,reject){

					json2csv({ data: codesArr, fields: ['trackingCode', 'encryptedCode'] }, function(err, csv) {
						if (!err){
							csv = csv.replace(/"/g,'')
							var base64Image = new Buffer(csv, 'binary').toString('base64');
							var mt = 'text/csv';

							if (values.plain) resolve(csv); //send plain csv instead of 
							else resolve({data:base64Image,mimetype:mt});
						}
						else reject(new Error(err));
					})

				})

			})
			.then(function(response){
				return [
					sails.models.codeblock.update({
						begin: idBegin,
						end: idEnd
					},{exported:true}),
					response
				];
			})
			.spread(function(codeBlocks,response){
				if (!codeBlocks.length) return q.reject(new Error("Impossibile aggiornare il codeblock"));
				else res.send(response);
			})
			.catch(function(err){
				res.customError(err);
			})
		}
	},
	invalidate: function(req,res){
		invalidateCode(req,res);
	},
	invalidateCheck: function(req,res){
		invalidateCode(req,res,{simulate:true});
	},
	resetAll: function(req,res){
		if (!sails.config.deyob.production){
			//reset codes, only in a dev environment
			q.all([
				sails.models.usercampaigns.destroy({}),
				sails.models.winstatus.destroy({}),
				sails.models.scanstatus.destroy({}),
				sails.models.user.update({},{points:0}),
				sails.models.userproducts.destroy({}),
				sails.models.wincode.destroy({}),
				sails.models.viralpointslog.destroy({})
			])
			.then(function(){
				res.send()
			})
			.catch(res.serverError);
		}
		else res.notFound();
	}
};


//PRIVATE METHODS
function CodeToObject(code){
	//manually populated values are not sent *sometimes* (sails bug?). Convert a sails object to true js object
	var awards;
	if (!!code){
		if (!!code.campaign){
			if (!!code.campaign.awards){
				var awards = code.campaign.awards;
				var campaignCopy = JSON.parse(JSON.stringify(code.campaign));
				campaignCopy.awards = awards;
				code.campaign = campaignCopy;
			}
		}

		return code;
	}
}

function invalidateCode(req,res,options){
	var values = actionUtil.parseValues(req);

	q()
	.then(function(){
		if (!values.code) return q.reject(errorCodes[1600]); //code mancante
		else return sails.models.wincode.findOne({qrcode:values.code});
	})
	.then(function(wincode){
		if (!wincode) return q.reject(errorCodes[1001]); //codice non appartenente a deyob
		else return utilsCode.checkInvalidatorValidity(wincode,req);
	})
	.then(utilsCode.populateWincode)
	.then(function(wincode){
		return utilsCode.invalidateWincode(wincode,options);
	})
	.then(function(wincode){
		// SEND CODE
		sails.log.silly("Sending Wincode to the client");
		var codeObj = _.cloneDeep(wincode);
		res.send(wincode);
		return codeObj;
	})
	.then(function(wincode){
		if ((!options) || (!options.simulate)) return utilsCode.updateWinlog(wincode,values);
		else return wincode;
	})
	.then(function(wincode){
		if ((!options) || (!options.simulate)) return utilsCode.updateInvalidateCodeGeo(wincode);
	})
	.catch(res.customError);	
}
