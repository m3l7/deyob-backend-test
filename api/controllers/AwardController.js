/**
 * AwardController
 *
 * @description :: Server-side logic for managing awards
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var fs = require('fs');
var path = require('path');

module.exports = {
	uploadImage: function(req,res){

	    var id = req.params.id;

	    req.file('file').upload({
	        dirname: '../../store/awards/',
	    },function(err,uploadedFiles){

	        if (!!err) res.serverError(err);
	        else if ((!!uploadedFiles) && (uploadedFiles.length)){


	            var filename = uploadedFiles[0].fd;
	            fs.renameSync(filename,filename+".png");

	            var fileapi = 'awards/'+path.basename(filename)+".png";

	            sails.models.award.findOne({id: id})
	            .then(function(award){
	                fs.unlink('store/'+award.image,function(err){
	                    sails.models.award.update({id: id},{image: fileapi})
	                    .then(function(award){
	                        res.send(sails.config.deyob.API.store+fileapi);
	                    })
	                    .catch(res.serverError);
	                })
	            })


	        }
	        else res.serverError();
	    });
	}
};

