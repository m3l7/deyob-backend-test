/**
 * NewsController
 *
 * @description :: Server-side logic for managing news
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');

module.exports = {
	find: function(req,res){

		var sort = actionUtil.parseSort(req) || "updatedAt DESC";

		// Lookup for records that match the specified criteria
		var query = sails.models.news.find()
		.where( actionUtil.parseCriteria(req) )
		.limit( actionUtil.parseLimit(req) )
		.skip( actionUtil.parseSkip(req) )
		.sort( sort )
		.populateAll();
		query.exec(function found(err, matchingRecords) {
		  if (err) return res.serverError(err);

		  res.ok(matchingRecords);
		});
	}
};
