/**
 * CampaignController
 *
 * @description :: Server-side logic for managing campaigns
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 var _=require('lodash');
 var path = require('path');
 var fs = require('fs');
 var q = require('q');
 var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');


module.exports = {
	userCampaigns: function(req,res){
		var user = req.user;

		sails.models.usercampaigns.find({user:user.id})
		.then(function(usercampaigns){
			var campaignsID = _.uniq(_.pluck(usercampaigns,'campaign'));

			var filter = actionUtil.parseCriteria(req);
			filter.id = campaignsID;
			var sort = actionUtil.parseSort(req) || "updatedAt DESC";

			return sails.models.campaign.find()
				.where( filter )
				.limit( actionUtil.parseLimit(req) )
				.skip( actionUtil.parseSkip(req) )
				.sort( sort )
				.populateAll();
		})
		.then(function(campaigns){
			if (campaigns){
				//POPULATE WINSTATUS

				var promises = [];
				campaigns.forEach(function(campaign){
					var defer = q.defer();
					sails.models.winstatus.findOne({campaign:campaign.id,user:req.user.id})
					.then(function(winstatus){
						if (winstatus) campaign.winStatus = winstatus;
						else campaign.winStatus = null;
						defer.resolve(campaign);
					})
					.catch(defer.reject);
					promises.push(defer.promise);
				})
				return q.all(promises);
			}
		})
		.then(function(campaigns){
			//fill children (for viral)
			var promises = [];
			campaigns.forEach(function(campaign){
				var defer = q.defer();
				if (campaign.type==5) {
					utilsCode.getScanChildren(campaign,req)
					.then(function(childrenObj){
						campaign.children = childrenObj;
						defer.resolve(campaign);
					})
					.catch(defer.reject);
				}
				else defer.resolve(campaign);

				promises.push(defer.promise);
			})
			return q.all(promises);
		})
		.then(function(campaigns){
			//build awards qrcodes
			return utilsCampaign.generateCampaignsInvalidateCodes(campaigns,req.user);
		})
		.then(function(campaigns){
			var promises = [];
			campaigns.forEach(function(campaign){
				promises.push(utilsCampaign.updateCampaignInfo(campaign));
			})
			return q.all(promises);
		})
		.then(function(campaigns){
			if (campaigns) res.send(campaigns);
		})
		.catch(res.serverError);
	},
	uploadImage: function(req,res){

	    var id = req.params.id;

	    req.file('file').upload({
	        dirname: '../../store/campaign/',
	    },function(err,uploadedFiles){
	        if (!!err) res.serverError(err);
	        else if ((!!uploadedFiles) && (uploadedFiles.length)){

	            var filename = uploadedFiles[0].fd;
	            fs.renameSync(filename,filename+".png");

	            var fileapi = 'campaign/'+path.basename(filename)+".png";

	            sails.models.campaign.findOne({id: id})
	            .then(function(campaign){
	            	if (campaign.image.indexOf('campaign/')!=-1) fs.unlink('store/'+campaign.image,updateCampaign);
	            	else updateCampaign();

	            	function updateCampaign(err){
	                    sails.models.campaign.update({id: id},{image: fileapi})
	                    .then(function(campaign){
	                        res.send(sails.config.deyob.API.store+fileapi);
	                    })
	                    .catch(res.serverError);
	                }
	            })


	        }
	        else res.serverError();
	    });
	},
	find: function(req,res){

		var values = actionUtil.parseValues(req);
		var sort = actionUtil.parseSort(req) || "updatedAt DESC";
		var parseCriteria = actionUtil.parseCriteria(req);
		delete parseCriteria.showexpired;
		delete parseCriteria.showAll;

		// Lookup for records that match the specified criteria
		var query = sails.models.campaign.find()
		.where( parseCriteria )
		.limit( actionUtil.parseLimit(req) )
		.skip( actionUtil.parseSkip(req) )
		.sort( sort );
		// TODO: .populateEach(req.options);
		query = actionUtil.populateEach(query, req);
		query.exec(function found(err, matchingRecords) {
		  if (err) return res.serverError(err);

		  var output = [];
		  matchingRecords.forEach(function(campaign){
			if(values.showAll){
		  		if ((!campaign.dateEnd) || (campaign.dateEnd>(new Date())) || (values.showexpired=="true")) output.push(campaign);
			}else{
			  	if (((!campaign.dateEnd) || (campaign.dateEnd>(new Date())) || (values.showexpired=="true")) && campaign.visible) output.push(campaign);
		 	}
		  })

		  res.ok(output);
		});
	},
	getViralRanking: function(req,res){
		var values = actionUtil.parseValues(req);
		var sort = actionUtil.parseSort(req) || "points DESC";

		var limit = values.limit || 20;
		if (limit>500) limit=500;
		
		q()
		.then(function(){
			if (!values.id)  return q.reject(errorCodes[1800]);
			else return sails.models.campaign.findOne(values.id);
		})	
		.then(function(campaign){
			if (!campaign)  return q.reject(errorCodes[1800]);
			else if (campaign.type!=5) return q.reject(errorCodes[1800]);
			else return sails.models.winstatus.find({campaign:campaign.id}).sort(sort).limit(limit).populate("user");
		})
		.then(function(winStatuses){
			winStatuses = _.filter(winStatuses,function(winStatus){
				return (winStatus.user) ? true : false;
			});
			users = _.map(winStatuses,function(winStatus){
				return {
					id:winStatus.user.id,
					social_username:winStatus.user.social_username,
					points:winStatus.points,
					first_name: winStatus.user.first_name,
					last_name: winStatus.user.last_name,
					children: winStatus.children
				};
			})
			res.send(users);	
		})
		.catch(res.customError);
	},
	getRangedViralRanking: function(req,res){
		var values = actionUtil.parseValues(req);
		var ranges = ["day","week","month"];
		var interval = req.params.interval;

		if ((!interval) || (ranges.indexOf(interval)==-1)) res.customError(errorCodes[1801]);
		else{
			var begin = new Date();
			var day = begin.getDay();
			if (day==0) day+=7;

			begin.setSeconds(0);
			begin.setMinutes(0);
			begin.setHours(0);
			if (interval=="week") begin.setDate(begin.getDate()-day+1);
			if (interval=="month") begin.setDate(1);

			sails.models.viralpointslog.find({
				createdAt: {">=": begin},
				campaign: req.params.id
			})
			.then(function(rows){
				var group = _.groupBy(rows,'user');
				var ret = _.map(group,function(user){
					return _.reduce(user,function(total,row){
						if (!total) return row;
						else{
							total.points+=row.points;
							return total;
						}
					})
				}) 
				ret = _.sortBy(ret,function(row){
					return row.points*(-1);
				});
				res.send(ret);
			})
		}
	}
};

