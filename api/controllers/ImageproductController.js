/**
 * ImagenewsController
 *
 * @description :: Server-side logic for managing awards
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {
 	create: function(req,res){

 		if ((!req.body) || (!req.body.product)) res.badRequest({summary: 'product id non valido',errorCode: 1500},{forceOutput: true});
 		else{
 			sails.models.product.findOne({id:req.body.product})
 			.then(function(product){
 				if (!product) res.badRequest({summary: 'product id non valido',errorCode: 1500},{forceOutput: true});
 				else return utilsUpload.uploadImageToStore(req,'product/');
 			})
 			.then(function(file){
 				if (!file) res.badRequest({summary: 'Nessuna immagine caricata',errorCode: 1501},{forceOutput: true});
 				else return sails.models.imageproduct.create({product:req.body.product,path:file});
 			})
 			.then(function(imagenews){
 				if (!imagenews) res.serverError("Impossibile creare l'immagine");
 				else res.send(imagenews);
 			})
 			.catch(res.serverError);
 		}

 	}

 };