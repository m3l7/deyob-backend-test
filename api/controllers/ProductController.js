/**
 * ProductController
 *
 * @description :: Server-side logic for managing products
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');


module.exports = {
	userProducts: function(req,res){
		//get user products
		var user = req.user;

		sails.models.userproducts.find({user:user.id})
		.then(function(userproducts){
			var productsID = _.uniq(_.pluck(userproducts,'product'));

			var filter = actionUtil.parseCriteria(req);
			filter.id = productsID;

			//search products
			return [
				sails.models.product.find()
					.where( filter )
					.limit( actionUtil.parseLimit(req) )
					.skip( actionUtil.parseSkip(req) )
					.sort( actionUtil.parseSort(req) )
					.populateAll(),
				userproducts
			];

		})
		.spread(function(products,userproducts){
			//add code to products
			var productsIndex = _.indexBy(products,"id");
			res.send(userproducts.map(function(userproduct){
				var ret = _.cloneDeep(productsIndex[userproduct.product]);
				ret.code = userproduct.code;
				return ret;
			}))
		})
		.catch(res.serverError);
	},
	create: function(req,res){

		var data = actionUtil.parseValues(req);

		utilsUpload.uploadImageToStore(req,'product/')

		.then(function(file){
			if (!!file) data.image = file;

			sails.models.product.create(data).exec(function created (err, newInstance) {
				if (err) return res.negotiate(err);
				else res.send(newInstance);
			});
			
		})
	}
};

