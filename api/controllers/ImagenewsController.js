/**
 * ImagenewsController
 *
 * @description :: Server-side logic for managing awards
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {
 	create: function(req,res){

 		if ((!req.body) || (!req.body.news)) res.badRequest({summary: 'News id non valido',errorCode: 1300},{forceOutput: true});
 		else{
 			sails.models.news.findOne({id:req.body.news})
 			.then(function(news){
 				if (!news) res.badRequest({summary: 'News id non valido',errorCode: 1300},{forceOutput: true});
 				else return utilsUpload.uploadImageToStore(req,'news/');
 			})
 			.then(function(file){
 				if (!file) res.badRequest({summary: 'Nessuna immagine caricata',errorCode: 1301},{forceOutput: true});
 				else return sails.models.imagenews.create({news:req.body.news,path:file});
 			})
 			.then(function(imagenews){
 				if (!imagenews) res.serverError("Impossibile creare l'immagine");
 				else res.send(imagenews);
 			})
 			.catch(res.serverError);
 		}

 	}

 };