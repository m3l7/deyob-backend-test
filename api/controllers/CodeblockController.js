/**
 * CodeblockController
 *
 * @description :: Server-side logic for managing codeblocks
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var q = require('q');
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');
var Promise = require("bluebird");

module.exports = {
    near: function(req, res) {
        var values = actionUtil.parseValues(req);
        if ((!values.begin) && (!values.end)) res.badRequest({
            summary: "Parametri begin e end mancanti",
            errorCode: 1201
        }, {
            forceOutput: true
        });

        values.begin = values.begin || values.end;
        values.end = values.end || values.begin;
        if (values.begin > values.end) values.begin = [values.end, values.end = values.begin][0];
        utilsCode.findNearValidBlock(values.begin, values.end)
            .then(function(nearBlock) {
                res.send(nearBlock);
            })
            .catch(res.serverError);

    },
    find: function(req, res) {

        var query = sails.models.codeblock.find()
            .where(actionUtil.parseCriteria(req))
            .limit(actionUtil.parseLimit(req))
            .skip(actionUtil.parseSkip(req))
            .sort(actionUtil.parseSort(req));
        // TODO: .populateEach(req.options);
        query = actionUtil.populateEach(query, req);
        query.exec(function found(err, matchingRecords) {
            if (err) return res.serverError(err);
            var blocks = [];
            matchingRecords.forEach(function(block) {
                //remove deleted or boundary blocks
                if (!block.deleted) blocks.push(block);
            })
            res.ok(blocks);
        });
    },
    update: function(req, res) {

        var pk = actionUtil.requirePk(req);
        var values = utilsReq.parseValues(req);

        //delete unsafe values
        delete values.begin;
        delete values.end;
        delete values.emptyAfter;
        delete values.emptyBefore;
        delete values.prev;
        delete values.next;
        delete values.updatedAt;
        delete values.createdAt;

        utilsUpload.uploadImageToStore(req, "actions/")
            .then(function(filepath) {
                //debugger
                if (filepath) values.actionImage = filepath;
                return sails.models.codeblock.findOne(pk)
                    .then(function(codeblock) {
                        if (codeblock.winners == values.winners) delete values.winners;
                    })
            })
            .then(function() {
                return sails.models.codeblock.update(pk, values);
            })
            .then(function(records) {

                var updatedRecord = records[0];

                if (values.winners) {
                    values.winners = parseInt(values.winners);

                    //random generate winners code
                    var winnersIds = [];
                    var range = updatedRecord.end - updatedRecord.begin + 1;
                    while (winnersIds.length < values.winners) {
                        var id = Math.floor(Math.random() * range) + updatedRecord.begin;
                        if (winnersIds.indexOf(id) == -1) winnersIds.push(id);
                    }
                    var loseIds = [];
                    for (var i = updatedRecord.begin; i <= updatedRecord.end; i++) {
                        if (winnersIds.indexOf(i) == -1) loseIds.push(i);
                    }

                    var winnersEnd = updatedRecord.begin + values.winners - 1;
                    if (winnersEnd > updatedRecord.end) winnersEnd = updatedRecord.end;
                    return [
                        updatedRecord,
                        sails.models.code.update({
                            trackingCode: winnersIds
                        }, {
                            winner: true
                        }),
                        sails.models.code.update({
                            trackingCode: loseIds
                        }, {
                            winner: false
                        }),
                    ];
                } else return [updatedRecord];
            })
            .spread(function(updatedRecord) {
                res.send(updatedRecord);
            })
            .catch(res.customError);

    },

    delete: function(req, res) {

        var idCodeblock = actionUtil.requirePk(req); // get id codeblock from request
        var values = actionUtil.parseValues(req); // get all value of request
        var codeblockToDelete;

        if (idCodeblock) {
            if ((!req.user.role == "admin") && (values.brand != req.user.id)) res.unauthorized();
            else {

                // get codeblock info 
                sails.models.codeblock.findOne(idCodeblock)
                    .then(function(codeblock) {
                        if (!codeblock) return Promise.reject(errorCodes[5002]);
                        else if (codeblock.assigned) return Promise.reject(errorCodes[5003]);
                        codeblockToDelete = codeblock;
                        // if codeblock is ready to delete
                        if (codeblock) return sails.models.codeblock.destroy(idCodeblock);

                    }).then(function(){
                        return sails.models.code.destroy({codeBlock : idCodeblock});
                    })
                    .then(function() {
                        return [utilsCode.findBlockByEnd(codeblockToDelete.prev), utilsCode.findBlockByBegin(codeblockToDelete.next)];
                    })
                    .spread(function(prevCodeblock, nextCodeblock) {

                        if (prevCodeblock) {
                            prevCodeblock.next = codeblockToDelete.next;
                            prevCodeblock.emptyAfter = codeblockToDelete.next - prevCodeblock.end - 1;
                            if(prevCodeblock.emptyAfter<=-1) prevCodeblock.emptyAfter = 0;
                        }
                        if (nextCodeblock) {
                            nextCodeblock.prev = codeblockToDelete.prev;
                            nextCodeblock.emptyBefore = nextCodeblock.begin - codeblockToDelete.prev - 1;
                        }
                        if (prevCodeblock && nextCodeblock) {
                            return [sails.models.codeblock.update(prevCodeblock.id, prevCodeblock), sails.models.codeblock.update(nextCodeblock.id, nextCodeblock)]
                        } else if (prevCodeblock && !nextCodeblock) {
                            return [sails.models.codeblock.update(prevCodeblock.id, prevCodeblock), null]

                        } else if (!prevCodeblock && nextCodeblock) {
                            return [null, sails.models.codeblock.update(nextCodeblock.id, nextCodeblock)]

                        } else if (!prevCodeblock && !nextCodeblock) {
                            return [null, null]
                        }
                    })
                    .spread(function(blockUpdatedPrev, blockUpdatedNext) {
                        res.ok();
                    })
                    .catch(res.customError);
            }
        }

    }




};