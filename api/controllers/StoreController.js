/**
 * StoreController
 *
 * @description :: Server-side logic for managing stores
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var path = require('path');


module.exports = {
	getFile: function(req,res){
		if (req.params.length){
			var filename = req.params[0];
			var file = path.join(__dirname, '../../store/',filename);
			res.sendfile(file,{},function(err){
				if (!!err) res.notFound();
			});			
		}
		else res.badRequest();
	}
};

