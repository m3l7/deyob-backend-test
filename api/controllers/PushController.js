/**
 * PushController
 *
 * @description :: Server-side logic for managing pushes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');


module.exports = {
	send: function(req,res){
		var values = actionUtil.parseValues(req);

		var channel,text,category;
		if ((values.user) && (!values.brand)){
			//send to user
			channel = ["user_"+values.user];
			category = "user";
		}	
		else if (!values.brand) {
			//send to all
			channel = ["deyob_2015"];
			category = "broadcast";
		}
		else if ((!values.campaign) && (!values.user)){
			//send to brand
			channel = ["brand_"+values.brand];
			category = "brand";
		}
		else if (values.campaign){
			//send to campaign
			channel = ["campaign_"+values.campaign];
			category = "campaign";
		}	
		else if (values.user){
			//send to user
			channel = ["user_"+values.user];
			category = "user";
		}	

		if ((!channel) || (!values.message)) res.badRequest(errorCodes[7001]);
		else utilsPush.sendPushToChannel(values.message,channel,category)
			.then(function(){
				res.ok();
			})
			.catch(res.serverError);
	}
};

