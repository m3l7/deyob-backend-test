// var nodemailer = require('nodemailer');

var nodemailer = require('nodemailer');
var q = require("q");
var Promise = require("bluebird");

var utilsMail = {
    sendResetMail: function(password,email){
        return q.promise(function(resolve,reject){
            if ((email) || (password)){
                var transporter = nodemailer.createTransport("SMTP",{
                    service: sails.config.deyob.email.service,
                    auth: {
                        user: sails.config.deyob.email.user,
                        pass: sails.config.deyob.email.pass
                    }
                });
                

                var message = "<p>La tua password DeYob e' stata reimpostata con successo.</p><p>La tua nuova password e': "+password+"</p>";
                transporter.sendMail({
                    from: 'deYob'+" <"+sails.config.deyob.email.user+">", // sender address
                    to: email, 
                    subject: 'deYob Reimpostazione Password', // Subject line
                    html: message
                },function(err,info){
                    if (err) reject(new Error(err));
                    else resolve();
                });
            }
            else reject(new Error('Invalid email or password'));
        });
    },
    sendActivationMail: function(email,token,cb){

        if ((!!email) && (!!token)){
            var transporter = nodemailer.createTransport("SMTP",{
                service: sails.config.deyob.email.service,
                auth: {
                    user: sails.config.deyob.email.user,
                    pass: sails.config.deyob.email.pass
                }
            });
            

            var activationLink = sails.config.deyob.registerUrl+"/"+token;
            var message = "<p>Thank you for registering.<p>Please click the link below to activate your account.</p><a href='"+activationLink+"'>"+activationLink+"</a>";
            transporter.sendMail({
                from: 'deYob'+" <filippo@crispybacon.it>", // sender address
                to: email, // list of receivers
                subject: 'deYob Activation Code', // Subject line
                html: message
            },function(err,info){
                if (!!cb){
                    if (!!err) cb(err); 
                    else cb();
                }
            });
        }
        else if (!!cb) cb('Invalid email or token');

    },
    sendCodesGeneratedMail: function(email,begin,end){
        return new Promise(function(resolve,reject){

            if ((!email) || (!begin) || (!end)) reject(new Error("email o blocco non valido"));

            var transporter = nodemailer.createTransport("SMTP",{
                service: sails.config.deyob.email.service,
                auth: {
                    user: sails.config.deyob.email.user,
                    pass: sails.config.deyob.email.pass
                }
            });
                
            var message = "<p>Blocco di codici generato correttamente</p><p>ID iniziale: "+begin+"</p><p>ID finale: "+end+"</p>";
            transporter.sendMail({
                from: 'deYob'+" <"+sails.config.deyob.email.user+">", // sender address
                to: email, 
                subject: 'DeYob generazione codici', // Subject line
                html: message
            },function(err,info){
                if (err) reject(new Error(err));
                else resolve();
            });
                
        })

    },
    sendBrandMail: function(email,user,pass,cb){

        if ((!!email) && (!!user) && (!!pass)){
            var transporter = nodemailer.createTransport("SMTP",{
                service: sails.config.deyob.email.service,
                auth: {
                    user: sails.config.deyob.email.user,
                    pass: sails.config.deyob.email.pass
                }
            });
            

            var message = "<p>Il tuo indirizzo e' stato attivato per accedere alla dashboard di deYob.(<a href='http://deyob.com/dashboard'></a>)</p><p>I tuoi dati di login sono:</p><p>Username: "+user+"</p><p>Password: "+pass+"</p>";
            transporter.sendMail({
                from: 'deYob'+" <filippo@crispybacon.it>", // sender address
                to: email, // list of receivers
                subject: 'deYob Login Information', // Subject line
                html: message
            },function(err,info){
                if (!!cb){
                    if (!!err) cb(err); 
                    else cb();
                }
            });
        }
        else if (!!cb) cb('email, user o password non validi');

    }
};

module.exports = utilsMail;