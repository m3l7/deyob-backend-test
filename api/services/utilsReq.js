var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');

module.exports = {
	parseValues: parseValues
}

function parseValues(req){
	//convert "null" to null
	var values = actionUtil.parseValues(req);

	for (var i in values){
		if (values[i]=="null") values[i] = null; 
	}

	return values;
}