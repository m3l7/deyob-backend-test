var q = require('q');
var instantWin = require("./campaigns/instantWin.js")
var fidelity = require("./campaigns/fidelity.js")
var lottery = require("./campaigns/lottery.js")
var collectionPoints = require("./campaigns/collectionPoints.js")
var viral = require("./campaigns/viral.js")
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var _ = require("lodash");
var geocoderProvider = 'google';
var httpAdapter = 'http';
var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter);
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');

var service = {
    findNearValidBlock: findNearValidBlock,
    isRangeValid: isRangeValid,
    toCode: toCode,
    toInt: toInt,
    editCodes: editCodes,
    createCodes: createCodes,
    populateCode: populateCode,
    checkCodeRelationsValidity: checkCodeRelationsValidity,
    updateUserProductAfterScan: updateUserProductAfterScan,
    updateCampaignAfterScan: updateCampaignAfterScan,
    checkCodeValidity: checkCodeValidity,
    doCampaignsLogic: doCampaignsLogic,
    updateCampaignInfo: updateCampaignInfo,
    exportCodes: exportCodes,
    updateScanLog: updateScanLog,
    updateScanStatus: updateScanStatus,
    updateCodeGeo: updateCodeGeo,
    getScanChildren: getScanChildren,
    generateInvalidateCodes: generateInvalidateCodes,
    updateWinlog: updateWinlog,
    updateInvalidateCodeGeo: updateInvalidateCodeGeo,
    populateWincode: populateWincode,
    invalidateWincode: invalidateWincode,
    checkInvalidatorValidity: checkInvalidatorValidity,
    getScanChildren: getScanChildren,
    findBlockByBegin: findBlockByBegin,
    findBlockByEnd: findBlockByEnd
};

module.exports = service;

function checkCodeRelationsValidity(code) {
    return q.promise(function(resolve, reject) {
        if (!code) reject(new Error("codice non presente"));
        else {
            if (code.campaign) code.campaign = utilsCampaign.checkExpired(code.campaign);
            if ((!code.product) && (!code.campaign)) reject(errorCodes[1003]);
            else if ((!code.product) && (code.campaign.expired)) reject(errorCodes[1004]);
            else {
                if ((code.campaign) && (code.campaign.expired)) delete code.campaign;
                resolve(code);
            }
        }
    })
}

function updateCampaignAfterScan(code) {
    sails.log.silly("Update campaign After Scan");
    return q.promise(function(resolve, reject) {
        if ((code.scanlog) && (code.scanlog.newUser) && (code.campaign)) {
            sails.models.campaign.update(code.campaign.id, {
                    uniqueUsers: code.campaign.uniqueUsers + 1
                })
                .then(function(campaigns) {
                    if (!campaigns.length) reject(new Error("Impossibile aggiornare il numero utenti della campagna"));
                    else {
                        code.campaign = campaigns[0];
                        resolve(code);
                    }
                })
                .catch(reject);
        } else resolve(code);
    })
}

function updateScanStatus(code, req) {
    // UPDATE SCAN STATUS AND CHECK IF WE ARE A NEW USER
    return q.promise(function(resolve, reject) {
        if ((!code) || (!code.campaign)) resolve(code);
        else q()
            .then(function() {
                return sails.models.scanstatus.create({
                    user: req.user.id,
                    code: code.id,
                    brand: ((code.brand) && (typeof code.brand == "object")) ? code.brand.id : null,
                    campaign: ((code.campaign) && (typeof code.campaign == "object")) ? code.campaign.id : null,
                    product: ((code.product) && (typeof code.product == "object")) ? code.product.id : null
                });
            })
            .then(function(log) {
                if (!log) return q.reject("Impossibile creare lo scan log");
                else {
                    code.scanstatus = log;
                    resolve(code);
                }
            })
            .catch(function(err) {
                sails.log.error(err);
                reject(err);
            });
    })

}

function updateScanLog(code,req){
	// UPDATE SCAN LOG AND CHECK IF WE ARE A NEW USER
	return q.promise(function(resolve,reject){
		if ((!code) || (!req)) resolve(code,req);
		else q().then(function(){
			//check if we are a new user
			if ((code.brand) && (code.campaign)) return sails.models.scanlog.findOne({campaign:code.campaign.id,user:code.user.id});
		})
		.then(function(log){
			if (!log) return true;
			else return false;
		})
		.then(function(newUser){
			return sails.models.scanlog.create({
				user:req.user.id,
				code: code.id,
				codeblock: code.codeBlock.id,
				brand: ((code.brand) && (typeof code.brand=="object")) ? code.brand.id : null,
				campaign: ((code.campaign) && (typeof code.campaign=="object")) ? code.campaign.id : null,
				product: ((code.product) && (typeof code.product=="object")) ? code.product.id : null,
				scanDate: new Date(),
				lat: req.body.lat,
				lng: req.body.lng,
				os: req.body.os,
				newUser: newUser,
				user_email: code.user.email,
				user_username: code.user.username,
				user_first_name: code.user.first_name,
				user_last_name: code.user.last_name,
				user_age: code.user.age,
				user_gender: code.user.gender,
				user_image: code.user.image
			});
		})
		.then(function(log){
			if (!log) return q.reject("Impossibile creare lo scan log");
			else {
				code.scanlog = log;
				resolve(code);
			}
		})
		.catch(function(err){
			sails.log.error(err);
			reject(err);
		});
	})
}

function updateUserProductAfterScan(code) {
    sails.log.silly("Update User Product After Scan");
    return q.promise(function(resolve, reject) {
        if (!code) reject("codice mancante");
        else if (!code.campaign) resolve(code);
        else if ((code.product) && (code.user)) {
            sails.models.userproducts.create({
                    user: code.user.id,
                    product: code.product.id,
                    code: code.id
                })
                .then(function(userproduct) {
                    if (!userproduct) reject("Impossibile aggiungere il prodotto all'utente");
                    else resolve(code);
                })
                .catch(reject);
        } else resolve(code);
    })
}

function updateCodeGeo(code) {
    return q.promise(function(resolve, reject) {
        if (!code) reject(new Error("codice mancante"));
        else {
            if ((code.scanlog.lat) && (code.scanlog.lng)) geocoder.reverse({
                lat: code.scanlog.lat,
                lon: code.scanlog.lng
            }, function(err, resp) {
                if ((!err) && (resp.length)) {
                    sails.models.scanlog.update({
                            id: code.scanlog.id
                        }, {
                            location: resp[0].city,
                            country: resp[0].country,
                            countryCode: resp[0].countryCode.toLowerCase(),
                            street: resp[0].streetName
                        })
                        .then(function(scanlogs) {
                            code.scanlog = scanlogs[0];
                            resolve(code);
                        })
                        .catch(function(err) {
                            sails.log.error('Error updating geocode to db: ' + err);
                            reject(new Error(err));
                        })
                } else if (err) {
                    sails.log.warn("Can't geocode: " + err);
                    resolve(code);
                } else resolve(code);
            })
            else resolve(code);
        }
    })
}


//INVALIDATE METHODS
function generateInvalidateCodes(code) {
    sails.log.silly("Generating Invalidate Codes");
    //find all awards and fill invalidate qrcodes
    return q.promise(function(resolve, reject) {
        if (!code) reject(new Error("invalid code"));
        else if (!code.campaign) resolve(code);
        else if ((!code.campaign.awards) || (!code.user)) reject(new Error("invalid code"));

        else {
            utilsCampaign.generateCampaignInvalidateCodes(code.campaign, code.user)
                .then(function(campaign) {
                    if (campaign) code.campaign = campaign;
                    resolve(code);
                })
                .catch(reject);
        }
    })
}

function updateInvalidateCodeGeo(code) {
    return q.promise(function(resolve, reject) {
        if (!code) reject(new Error("codice mancante"));
        else {
            if ((code.winlog.lat) && (code.winlog.lng)) geocoder.reverse({
                lat: code.winlog.lat,
                lon: code.winlog.lng
            }, function(err, resp) {
                if ((!err) && (resp.length)) {
                    sails.models.winlog.update({
                            id: code.winlog.id
                        }, {
                            location: resp[0].city,
                            country: resp[0].country,
                            countryCode: resp[0].countryCode.toLowerCase(),
                            street: resp[0].streetName
                        })
                        .then(function(scanlogs) {
                            code.winlog = scanlogs[0];
                            resolve(code);
                        })
                        .catch(function(err) {
                            sails.log.error('Error updating geocode to db: ' + err);
                            reject(new Error(err));
                        })
                } else if (err) {
                    sails.log.warn("Can't geocode: " + err);
                    resolve(code);
                } else resolve(code);
            })
            else resolve(code);
        }
    })
}

function populateWincode(wincode) {
    return q.promise(function(resolve, reject) {
        if (!wincode) reject(new Error("wincode non presente"));
        else {
            q()
                .then(function() {
                    return [
                        sails.models.campaign.findOne(wincode.campaign),
                        sails.models.user.findOne(wincode.brand),
                        sails.models.user.findOne(wincode.user),
                        sails.models.award.findOne(wincode.award),
                        sails.models.award.find({
                            campaign: wincode.campaign
                        }),
                        sails.models.winstatus.findOne({
                            user: wincode.user,
                            campaign: wincode.campaign
                        })
                    ]
                })
                .spread(function(campaign, brand, user, award, awards, winstatus) {
                    wincode.campaign = campaign;
                    wincode.brand = brand;
                    wincode.award = award;
                    wincode.user = user;
                    if (wincode.campaign) wincode.campaign.winStatus = winstatus;
                    if (wincode.campaign) {
                        wincode.campaign.brand = brand;
                        wincode.campaign.awards = awards;
                    }
                    resolve(wincode);
                })
                .catch(reject)
        }
    })
}

function updateWinlog(wincode, values) {
    sails.log.silly("Update winlog");
    return q.promise(function(resolve, reject) {
        if (!wincode) reject(new Error("Invalid wincode"));
        else if ((!wincode.user) || (!wincode.brand) || (!wincode.campaign) || (!wincode.award)) reject(new Error("Invalid wincode populated models"));
        else {
            sails.models.winlog.create({
                    user: wincode.user.id,
                    brand: wincode.brand.id,
                    campaign: wincode.campaign.id,
                    product: (wincode.product) ? wincode.product.id : null,
                    code: wincode.id,
                    award: wincode.award.id,
                    os: values.os,
                    scanDate: new Date(),
                    lng: values.lng,
                    lat: values.lat
                })
                .then(function(winlog) {
                    wincode.winlog = winlog;
                    resolve(wincode);
                })
                .catch(reject);
        }
    })
}

function checkInvalidatorValidity(wincode, req) {
    return q.promise(function(resolve, reject) {
        if ((!wincode) || (!req)) reject(new Error("Invalid wincode or req"));
        else {
            var brandID = (typeof wincode.brand == "object") ? wincode.brand.id : wincode.brand;
            sails.models.invalidator.findOne({
                    user: req.user.id,
                    brand: brandID
                })
                .then(function(invalidator) {
                    if (invalidator) resolve(wincode);
                    else reject(errorCodes[1601]);
                })
                .catch(reject);
        }
    })
}



//CODEBLOCK METHODS
function findNearValidBlock(begin, end) {
    //find a near block where we can put a new [begin,end] block
    var deferred = q.defer();

    if ((!begin) || (!end)) deferred.reject('parametri mancanti');
    else {
        //find backward
        var range = parseInt(end) - parseInt(begin) + 1;

        sails.models.codeblock.find({
                where: {
                    begin: {
                        '<=': begin
                    },
                    emptyAfter: {
                        '>=': range
                    }
                },
                sort: 'begin DESC',
                limit: 1
            })
            .then(function(blockBefore) {
                //find forward
                if (blockBefore.length) deferred.resolve(blockBefore[0]);
                else return sails.models.codeblock.find({
                    where: {
                        begin: {
                            '>': begin
                        },
                        emptyAfter: {
                            '>=': range
                        }
                    },
                    sort: 'begin ASC',
                    limit: 1
                });
            })
            .then(function(blockAfter) {
                if (typeof blockAfter != "undefined") {
                    if (blockAfter.length) deferred.resolve(blockAfter[0]);
                    else return sails.models.codeblock.find({
                        where: {
                            next: 0
                        },
                        limit: 1
                    }); //can't find space at the beginning. Go to the end.
                }
            })
            .then(function(block) {
                if (typeof block != "undefined") {
                    if (!block) deferred.reject("Errore interno del database. Impossibile trovare l'ultimo blocco");
                    else deferred.resolve(block[0]);
                }
            })
            .catch(deferred.reject);

    }
    return deferred.promise;
}

//CODEBLOCK METHODS
function findBlockByBegin(beginField) {

    return q.promise(function(resolve, reject) {
        sails.models.codeblock.find({
                    begin: beginField
            })
            .then(function(block) {
                if (block && block.length == 1) {
                    resolve(block[0]);
                } else {
                    reject;
                }
            })
            .catch(reject);
    })
}



function findBlockByEnd(endField) {

    return q.promise(function(resolve, reject) {
        sails.models.codeblock.find({
                    end: endField
            })
            .then(function(block) {
                if (block && block.length == 1) {
                    resolve(block[0]);
                } else {
                    reject;
                }

            })
            .catch(reject);
    })
}



function isRangeValid(begin, end) {
    var deferred = q.defer();
    if ((!begin) || (!end)) defer.reject('Parametri mancanti');
    else {
        findNearValidBlock(begin, end)
            .then(function(nearBlock) {
                var valid = false;
                if ((begin > nearBlock.end) && (end < nearBlock.next)) valid = true;
                deferred.resolve(valid);
            })
            .catch(deferred.reject);
    }
    return deferred.promise;
}

function toCode(number) {
    if (typeof number == "number") {
        number = number.toString(16);
        while (number.length != 11) number = '0' + number;
        return number;
    }
}

function toInt(code) {
    if (!!code) return parseInt(code);
    // if (!!code) return parseInt(code,16);
}

//POPULATE METHODS
function populateCode(code, req) {
    sails.log.silly("Populating code");
    //populate all code relations
    return q.promise(function(resolve, reject) {
        if (!code) reject(errorCodes[1001]);
        else {
            populateCodeBlock(code)
                .then(function(code) {
                    return populateCodeCampaign(code, req);
                })
                .then(populateCodeBrand)
                .then(populateCodeProduct)
                .then(populateCodeUser)
                .then(function(code) {
                    return populateCodeUser(code, req);
                })
                .then(resolve)
                .catch(reject);
        }
    });
}

function populateCodeBlock(code) {
    return q.promise(function(resolve, reject) {
        if ((code) && (code.codeBlock)) {
            sails.models.codeblock.findOne(code.codeBlock)
                .then(function(codeBlock) {
                    if (!codeBlock) reject(new Error("Code block non trovato"));
                    else {
                        code.codeBlock = codeBlock;
                        resolve(code);
                    }
                })
                .catch(function(err) {
                    reject(new Error(err));
                })
        } else reject(new Error("Codice non presente"));
    })
}

function populateCodeUser(code, req) {
    if ((code) && (req) && (req.user)) code.user = req.user;
    return code;
}

function populateCodeCampaign(code, req) {
    //populate code with campaign model
    return q.promise(function(resolve, reject) {
        if (!code) reject(errorCodes[1001]);
        else if ((!code.codeBlock) || (!code.codeBlock.campaign)) resolve(code);
        else {
            sails.models.campaign.findOne({
                    id: code.codeBlock.campaign
                })
                .populate("brand")
                .populate("awards")
                .then(function(campaign) {
                    if (!campaign) delete code.campaign;
                    else {
                        code.campaign = campaign;
                        return sails.models.winstatus.findOne({
                            campaign: campaign.id,
                            user: req.user.id
                        });
                    }
                })
                .then(function(winstatus) {
                    if ((code.campaign) && (winstatus)) code.campaign.winStatus = winstatus;
                    else code.campaign.winStatus = null;
                    var ret = [code];
                    if ((code.campaign) && (code.campaign.type == 5)) ret.push(getScanChildren(code.campaign, req));
                    return ret;
                })
                .spread(function(code, childrenObj) {
                    if ((code.campaign) && (childrenObj)) {
                        code.campaign.children = childrenObj;
                    }
                    resolve(code);
                })
                .catch(reject);
        }
    })
}

function populateCodeBrand(code) {
    return q.promise(function(resolve, reject) {
        if (!code) reject(errorCodes[1001]);
        else if ((!code.codeBlock) || (!code.codeBlock.brand)) resolve(code);
        else if ((code.campaign) && (code.campaign.brand)) {
            code.brand = code.campaign.brand;
            resolve(code);
        } else {
            sails.models.user.findOne({
                    role: "brand",
                    id: code.brand
                })
                .then(function(brand) {
                    if (!!brand) code.brand = brand;
                    resolve(code);
                })
                .catch(reject);
        }
    })
}

function populateCodeProduct(code) {
    return q.promise(function(resolve, reject) {
        if (!code) reject(errorCodes[1001]);
        else if ((!code.codeBlock) || (!code.codeBlock.product)) resolve(code);
        else {
            sails.models.product.findOne({
                    id: code.codeBlock.product
                })
                .populate("images")
                .then(function(product) {
                    if (!!product) code.product = product;
                    resolve(code);
                })
                .catch(reject);
        }
    })
}

function exportCodes(idBegin, idEnd) {
    return q.promise(function(resolve, reject) {
        sails.models.code.update({
                trackingcode: {
                    '>=': idBegin,
                    '<=': idEnd
                },
            }, {
                exported: true
            })
            .then(resolve)
            .catch(reject);
    })
}

function editCodes(idBegin, idEnd, values, req) {
    return q.promise(function(resolve, reject) {
        q()
            .then(function() {
                if ((!idBegin) || (!idEnd) || (idBegin > idEnd)) reject(new Error("Range non valido"));
            })
            .then(function() {
                return utilsUpload.uploadImageToStore(req, "actions/");
            })
            .then(function(file) {
                if (file) {
                    if (!values) values = {};
                    if (!values.action) values.action = "";
                    values.action += '<img src="' + file + '">';
                }
            })
            .then(function() {
                return sails.models.codeblock.find({
                    where: {
                        begin: {
                            "<=": idBegin
                        },
                        end: {
                            ">=": idEnd
                        }
                    }
                });
            })
            .then(function(blocks) {
                if (!blocks.length) reject(new Error("Range non valido"));
                else {
                    var block = blocks[0];
                    var blockCopy = _.cloneDeep(block);
                    //remove current block and create new splitted blocks
                    return [
                        blockCopy,
                        sails.models.codeblock.destroy(block.id)
                    ];
                }
            })
            .spread(function(block) {
                return [
                    createSplittedBlock(block, idBegin, idEnd, values),
                    createSplittedBlock(block, block.begin, idBegin - 1), //new left block
                    createSplittedBlock(block, idEnd + 1, block.end) //new right block
                ];
            })
            .spread(function(newcodeblock) {
                return sails.models.code.update({
                    trackingCode: {
                        ">=": idBegin,
                        "<=": idEnd
                    },
                }, {
                    codeBlock: newcodeblock.id
                });
            })
            .then(function() {
                resolve();
            })
            .catch(function(err) {
                reject(new Error(err));
            })
    })
}

function createSplittedBlock(block, begin, end, values) {
    //create block and adjust left and right refs if necessary
    return q.promise(function(resolve, reject) {
        if ((!block) || (!begin) || (!end)) reject(new Error("block,begin o end mancanti"));
        else if (begin > end) resolve();
        else {
            var blockCopy = _.cloneDeep(block);
            if (values) {
                blockCopy = _.merge(blockCopy, values);
                blockCopy.assigned = true;
                if (values.action) {
                    blockCopy.action = true;
                    blockCopy.actionValue = values.action;
                }
            }
            delete blockCopy.id;
            if (blockCopy.begin != begin) {
                //adjust left refs
                blockCopy.emptyBefore = 0;
                blockCopy.prev = begin - 1;
                blockCopy.begin = begin;
            }
            if (blockCopy.end != end) {
                //adjust right refs
                blockCopy.emptyAfter = 0;
                blockCopy.next = end + 1;
                blockCopy.end = end;
            }

            sails.models.codeblock.create(blockCopy)
                .then(resolve)
                .catch(function(err) {
                    reject(new Error(err));
                })
        }
    })
}


function createCodes(idBegin, idEnd, interval, codeBlock) {
    //create codes (split create operation in blocks)
    return q.promise(function(resolve, reject) {
        var end = ((idBegin + interval) > idEnd) ? idEnd : (idBegin + interval);
        console.log(idBegin + " ---- " + idEnd + " ---- " + end);
        var filter = [];
        for (var i = idBegin; i <= end; i++) {
            filter.push(_.merge({
                trackingCode: i
            }, {
                codeBlock: codeBlock
            }, genCipherCodes(i)));
        }
        sails.models.code.create(filter)
            .then(function(codes) {
                if (idEnd <= end) resolve();
                else return createCodes((end + 1), idEnd, interval, codeBlock);
            })
            .then(resolve)
            .catch(reject);
    })
}


function getScanChildren(campaign, obj) {
    //get children of viral campaign, based on campaignId and obj (can be req or userId)
    return q.promise(function(resolve, reject) {
        if ((campaign) && (obj)) {
            var userId = (typeof obj == "number") ? obj : obj.user.id;
            var campaignId = (typeof campaign == "number") ? campaign : campaign.id;

            sails.models.winstatus.find({
                    campaign: campaignId,
                    parent: userId
                })
                .populate("user")
                .limit(100)
                .then(function(statuses) {
                    var ret = {
                        total: statuses.length,
                        data: []
                    };
                    statuses.forEach(function(winstatus) {
                        ret.data.push({
                            image: winstatus.user.image,
                            first_name: winstatus.user.first_name,
                            last_name: winstatus.user.last_name
                        });
                    })
                    resolve(ret);
                })
                .catch(function(err) {
                    reject(new Error(err));
                })
        } else reject(new Error("campagna o req mancanti"));
    })
}


//CAMPAIGN RELATED METHODS
function getCampaignModule(code) {
    if (!code.campaign) return false;
    else if (code.campaign.type == 1) return lottery;
    else if (code.campaign.type == 2) return instantWin;
    else if (code.campaign.type == 3) return collectionPoints;
    else if (code.campaign.type == 4) return fidelity;
    else if (code.campaign.type == 5) return viral;
    else return false;
}

function updateCampaignInfo(code) {
    if (!code) return q.reject(new Error("codice mancante"));
    else if (!code.campaign) return q.resolve(code);
    else return q.promise(function(resolve, reject) {
        utilsCampaign.updateCampaignInfo(code.campaign)
            .then(function(campaign) {
                code.campaign = campaign;
                resolve(code);
            })
            .catch(reject);
    })
}

function checkCodeValidity(code, req) {
    sails.log.silly("Checking code validity");
    if (!code) throw new Error("codice mancante");
    else if (!code.campaign) return q.resolve(code);
    else {
        var campaignModule = getCampaignModule(code);
        if (!campaignModule) throw new Error("tipo di campagna non valido");
        else return campaignModule.checkCodeValidity(code, req);
    }
}

function doCampaignsLogic(code) {
    sails.log.silly("Doing campaign logic");
    if (!code) throw new Error("codice mancante");
    else if (!code.campaign) return q.resolve(code);
    else {
        var campaignModule = getCampaignModule(code);
        if (!campaignModule) throw new Error("tipo di campagna non valido");
        else return campaignModule.scanCode(code);
    }
}

function invalidateWincode(wincode, options) {
    sails.log.silly("Invalidate wincode logic");

    return q.promise(function(resolve, reject) {
        if (!wincode) reject(new Error("codice mancante"));

        var campaignModule = getCampaignModule(wincode);
        if (!campaignModule) reject(new Error("tipo di campagna non valido"));
        else campaignModule.invalidateWincode(wincode, options)
            .then(function(wincode) {
                if (wincode.campaign) delete wincode.campaign.winStatus;
                resolve(wincode);
            })
            .catch(reject);
    })

}


// PRIVATE METHODS
function encrypt(text, password) {
    var cipher = crypto.createCipher(algorithm, password)
    cipher.setAutoPadding(false);
    var crypted = cipher.update(text, 'utf8', 'base64')
    crypted += cipher.final('base64');
    return crypted;
}

function decrypt(text, password) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text, 'base64', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

function genCipherCodes(trackingCode) {
    //generate code. Out: {trackingCode, clearCode, encCode, cypherPass}

    trackingCode = trackingCode.toString(36);
    while (trackingCode.length < 11) trackingCode = '0' + trackingCode;
    var d = new Date();

    var day = d.getDate().toString();
    day = (day.length == 1) ? '0' + day : day;
    var month = (d.getMonth() + 1).toString();
    month = (month.length == 1) ? '0' + month : month;

    var date = day + month + d.getFullYear().toString();
    var key = utilsCrypt.uid(13);
    var clearCode = date + trackingCode + key;
    var cipherPass = utilsCrypt.uid(10);
    var encryptedCode = encrypt(clearCode, cipherPass);
    encryptedCode = encryptedCode.slice(0, -1);

    return {
        encryptedCode: encryptedCode,
        clearCode: clearCode,
        cipherPass: cipherPass
    };
};