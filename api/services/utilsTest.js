var q = require('q');
var fixtures = require('sails-fixtures');

module.exports = {
	loadFixtures: loadFixtures,
	clearDatabase: clearDatabase,
	clearAndLoad: clearAndLoad
}

function loadFixtures(fixturesArray){
	//load array of fixtures into the database
	if ((fixturesArray) && (fixturesArray.length)){
		return q.all(fixturesArray.map(function(fixture){
		  return q.Promise(function(resolve,reject){
		  	fixtures.loadFromFiles({'dir':fixture,'pattern':'*.*'},function(err,files){
		  		if (!files) reject("loading fixtures from "+fixture+" :no files loaded");
		  		else resolve();
		  	});
		  })
		}))
	}
}
function clearDatabase(){
	var promises = [];
	for (var i in sails.models){
	  promises.push(sails.models[i].destroy());
	}
	return q.all(promises);
}
function clearAndLoad(fixtures){
	return clearDatabase()
		.then(function(){
			return loadFixtures(fixtures);
		})
		.catch(function(err){
			console.log(err);
			return q.reject(err);
		})
}