var q = require("q");
var _ = require("lodash");
var instantWin = require("./campaigns/instantWin.js")
var fidelity = require("./campaigns/fidelity.js")
var lottery = require("./campaigns/lottery.js")
var collectionPoints = require("./campaigns/collectionPoints.js")
var viral = require("./campaigns/viral.js")



module.exports = {
	generateCampaignInvalidateCodes: generateCampaignInvalidateCodes,
	generateCampaignsInvalidateCodes: generateCampaignsInvalidateCodes,
	checkExpired: checkExpired,
	updateCampaignInfo: updateCampaignInfo
}

function generateCampaignsInvalidateCodes(campaigns,user){
	return q.promise(function(resolve,reject){
		if ((!campaigns) || (!user)) reject(new Error("Invalid campaigns or user"));

		var promises = [];
		campaigns.forEach(function(campaign){
			promises.push(generateCampaignInvalidateCodes(campaign,user));
		})

		q.all(promises)
		.then(function(campaigns){
			resolve(campaigns);
		})
		.catch(reject);
		
	})
}

//GENERATE INVALIDATION CODES FOR EACH AWARD/USER, IF NOT PRESENTS
function generateCampaignInvalidateCodes(campaign,user){
	return q.promise(function(resolve,reject){
		q()
		.then(function(){
			if ((!campaign) || (!user)) throw new Error("Invalid campaign or user");

			var campaignId = (typeof campaign=="object") ? campaign.id : campaign;
			var userId = (typeof user=="object") ? user.id : user;

			var promises = [
				userId,
				sails.models.award.find({campaign:campaignId}),
				sails.models.wincode.find({campaign:campaignId,user:userId})
			];

			if (typeof campaign=="object") promises.push(campaign);
			else promises.push(sails.models.campaign.findOne(campaign));

			return promises;
		})
		.spread(function(userId, awards,codes,campaign){
			//create missing codes
			var codesIndexed = _.indexBy(codes,"award");
			var promises = [];

			var brandId = (typeof campaign.brand == "object") ? campaign.brand.id : campaign.brand;

			awards.forEach(function(award){
				if (!codesIndexed[award.id]) promises.push(sails.models.wincode.create({
					campaign: campaign.id,
					award: award.id,
					user: userId,
					qrcode: utilsCrypt.uid(32),
					brand: brandId
				}));
			});

			return [
				codes,
				q.all(promises),
				awards,
				campaign
			];
		})
		.spread(function(oldcodes,newcodes,awards,campaign){
			//build filled awards array
			var codes = oldcodes.concat(newcodes);
			var codesIndexed = _.indexBy(codes,"award");
			awards.forEach(function(award){
				award.qrcode = codesIndexed[award.id].qrcode;
			})

			campaign.awards = awards;

			resolve(campaign);
		})
		.catch(reject);
	})
} 

function checkExpired(campaign){

	//check if campaigns are expired
	var now = new Date();
	if (!campaign.dateEnd) campaign.expired = false;
	else if (campaign.dateEnd<now) campaign.expired = true;
	else campaign.expired = false; 
	return campaign;

}
function updateCampaignInfo(campaign){
	sails.log.silly("Updating campaign info");
	return q.promise(function(resolve,reject){
		if (!campaign) reject(new Error("campagna non presente"));
		else{
			var campaignModule = getCampaignModule(campaign);
			if (!campaignModule) throw new Error("tipo di campagna non valido");
			else if (campaignModule.updateCampaignInfo) campaignModule.updateCampaignInfo(campaign)
				.then(resolve)
				.catch(reject);
			else resolve(campaign);
		}
	})
}


function getCampaignModule(campaign){
	if (!campaign) return false;
	else if (campaign.type==1) return lottery;
	else if (campaign.type==2) return instantWin;
	else if (campaign.type==3) return collectionPoints;
	else if (campaign.type==4) return fidelity;
	else if (campaign.type==5) return viral;
	else return false;
}