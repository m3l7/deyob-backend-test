var _ = require('lodash');

var codes = [
	{
		errorCode: 1001,
		status: 400,
		summary: "Codice non appartenente a DeYob"

	},
	{
		errorCode: 1002,
		status: 400,
		summary: "Codice gia assegnato"
	},
	{
		errorCode: 1003,
		status: 400,
		summary: "Codice non valido"
	},
	{
		errorCode: 1004,
		status: 400,
		summary: "Codice con campagna scaduta"
	},
	{
		errorCode: 1110,
		status: 400,
		summary: "Email mancante"
	},
	{
		errorCode: 1111,
		status: 400,
		summary: "Email non esistente"
	},
	{
		errorCode: 1112,
		status: 400,
		summary: "Email non presente nel profilo"
	},
	{
		errorCode: 1113,
		status: 400,
		summary: "La password corrente non e' corretta"
	},
	{
		errorCode: 1114,
		status: 400,
		summary: "La password corrente e la nuova password non sono state fornite"
	},

	// INVALIDATOR API
	{
		errorCode: 1600,
		status: 400,
		summary: "QRcode mancante"
	},
	{
		errorCode: 1601,
		status: 400,
		summary: "L'utente non e' un invalidatore per il brand"
	},
	{
		errorCode: 1602,
		status: 400,
		summary: "L'utente non ha sufficienti punti"
	},
	{
		errorCode: 1603,
		status: 400,
		summary: "user o brand mancanti"
	},
	{
		errorCode: 1700,
		status: 400,
		summary: "codice sms o userId mancante"
	},
	{
		errorCode: 1701,
		status: 400,
		summary: "codice sms non valido"
	},
	{
		errorCode: 1702,
		status: 400,
		summary: "userId non valido"
	},
	{
		errorCode: 1703,
		status: 400,
		summary: "numero di telefono non valido"
	},
	{
		errorCode: 1704,
		status: 400,
		summary: "numero di telefono gia utilizzato"
	},

	//CAMPAIGN API
	{
		errorCode: 1800,
		status: 400,
		summary: "campagna non valida"
	},
	//viral campaigns
	{
		errorCode: 1801,
		status: 400,
		summary: "intervallo non valido"
	},

	//BLOCK API
	{
		errorCode: 5001,
		status: 400,
		summary: "Range non valido"
	},
	{
		errorCode: 5002,
		status: 400,
		summary: "Id del blocco da eliminare non esistente"
	},
	{
		errorCode: 5003,
		status: 400,
		summary: "Impossibile eliminare un codeblock gia' assegnato"
	},

	//USER API
	{
		errorCode: 6001,
		status: 400,
		summary: "Utente in lock. Operazione gia in corso"
	},

	//PUSH API
	{
		errorCode: 7001,
		status: 400,
		summary: "Invalid push parameters"
	},



];

module.exports = _.indexBy(codes,"errorCode");
