// initialize and export parse for push notifications
var Parse = require('node-parse-api').Parse,
	config = require('../../loadSailsrc'),
	parseApp = new Parse(config.config.deyob.parse.appID, config.config.deyob.parse.masterKey),
	Promise = require("bluebird"),
	q = require("q");

var service = {
	parseApp: parseApp,
	sendPushToChannel: sendPushToChannel
}

module.exports = service;

function sendPushToChannel(text, channels, type, retryIndex){
	//send push notification, retry if fail
	if (sails.config.deyob.parse.disabled) return Promise.resolve();
	else{
		if (!retryIndex) retryIndex = 0;
		var nRetries = 4;

		return sendPushToChannelSingle(text,channels,type)
			.catch(function(err){
				sails.log.silly("can't send push: "+err);
				if (retryIndex<nRetries) return sendPushToChannel(text,channels,type,retryIndex+1);
			});
	}

}

function sendPushToChannelSingle(text, channels, type){
	//send push notification to specific channels, with an optional category
	return q.promise(function(resolve,reject){
		
		service.parseApp.sendPush({
		  channels: channels,
		  data:{
		    badge: 'increment',
		    alert: text,
		    category: type
		  }
		}, function(err, res) {
		    if (err) reject(err);
		    else {
		    	sails.log.silly("Sent push: "+channels+" message: "+text);
		    	resolve();
		    }
		});
	})
}