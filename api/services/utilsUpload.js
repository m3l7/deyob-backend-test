var q = require('q');
var fs = require('fs');
var path = require('path');

module.exports = {
	uploadImageToStore: function(req,folder){
		//upload to store/folder, returns a clean path with a promise
		var deferred = q.defer();

		if (!folder) folder = '';
		req.file('file').upload({
		    dirname: '../../store/'+folder,
		},function(err,uploadedFiles){
		    if (!!err) deferred.reject(err);
		    else if ((!!uploadedFiles) && (uploadedFiles.length)){

		        var filename = uploadedFiles[0].fd;
		        fs.renameSync(filename,filename+".png");

		        var fileapi = folder+path.basename(filename)+".png";

		        deferred.resolve(fileapi);
		    }
		    else deferred.resolve("");
		});
		return deferred.promise;
	}
}