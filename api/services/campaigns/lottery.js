//lottery campaign -- type 1
var q = require("q");

module.exports = {
	checkCodeValidity: checkCodeValidity,
	scanCode: scanCode,
	invalidateWincode: invalidateWincode
}


function checkCodeValidity(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice non presente");
		else if (!code.codeBlock) reject(new Error("Blocco non presente"));
		else if (!code.codeBlock.exported) reject(errorCodes[1003]);
		else sails.models.scanstatus.findOne({code:code.id})
			.then(function(scanstatus){
				if (scanstatus) {
					var errorCode = errorCodes[1002];
					if(errorCode.action) delete errorCode.action;
					if(errorCode.actionVideoID) delete errorCode.actionVideoID;
					if(code.codeBlock.action){
						if(code.codeBlock.actionType==1){ //YOUTUBE
							errorCode.actionVideoID = getVideoId(code.codeBlock.actionText);
            				errorCode.action = "<iframe src='https://www.youtube.com/embed/"+errorCode.actionVideoID+"' frameborder='0' allowfullscreen></iframe>";
            				errorCode.action = normalizeVideoAction(errorCode.action);
						}else if(code.codeBlock.actionType==2){ //HTML
							errorCode.action = code.codeBlock.actionText;
						}
					}					
					reject(errorCode);
				} else resolve(code);
			})
	})
}
function scanCode(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice mancante");
		else{
			q().then(function(){
				return sails.models.usercampaigns.create({user:code.user.id,campaign:code.campaign.id,winStatus:null});
			})
			.then(function(){
				resolve(code);
			})
			.catch(reject);
		}
	})
}
function invalidateWincode(wincode){
	sails.log.silly("Invalidate lottery wincode");
	return q.promise(function(resolve,reject){
		resolve(wincode);
	})
}

function normalizeVideoAction(action){
  if (action){

    var ret = action;

    var containerUp = '<html><head><style>p{text-align: center;}</style><meta name="viewport" content="width=device-width, user-scalable=no,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"></head><body><div style="width:100%;">';
    var containerDown = '</div></body></html>';
    ret = containerUp+ret+containerDown;

    return ret;
  }
}

function getVideoId(action){
  if (action){
    var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    var match = action.match(myregexp);
    if ((match) && (match.length>1)) return match[1];
    else return "";
  }
}