//instant win campaign -- type 2
var q = require("q");

module.exports = {
	checkCodeValidity: checkCodeValidity,
	scanCode: scanCode,
	invalidateWincode: invalidateWincode
}


function checkCodeValidity(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice non presente");
		else if (!code.codeBlock) reject(new Error("Blocco non presente"));
		else if (!code.codeBlock.exported) reject(errorCodes[1003]);
		else if ((code.campaign) && (!code.winner)) resolve(code);
		else sails.models.scanstatus.findOne({code:code.id})
			.then(function(scanstatus){
				if ((scanstatus) && (code.winner)) {
					var errorCode = errorCodes[1002];
					if(errorCode.action) delete errorCode.action;
					if(errorCode.actionVideoID) delete errorCode.actionVideoID;
					if(code.codeBlock.action){
						if(code.codeBlock.actionType==1){ //YOUTUBE
							errorCode.actionVideoID = getVideoId(code.codeBlock.actionText);
            				errorCode.action = "<iframe src='https://www.youtube.com/embed/"+errorCode.actionVideoID+"' frameborder='0' allowfullscreen></iframe>";
            				errorCode.action = normalizeVideoAction(errorCode.action);
						}else if(code.codeBlock.actionType==2){ //HTML
								errorCode.action = code.codeBlock.actionText;
						}
					}					
					reject(errorCode);
				} else resolve(code);
			})
	})
}
function scanCode(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice non presente");
		else q()
		.then(function(){

			//update winStatus
			var points = (code.campaign.winStatus) ? code.campaign.winStatus.points : 0;
			if (code.winner) points++;
			if (!code.campaign.winStatus) return sails.models.winstatus.create({
				points: points,
				user: code.user.id,
				campaign: code.campaign.id
			});
			else return sails.models.winstatus.update({
				user: code.user.id,
				campaign: code.campaign.id,
			},{points: points});
		})
		.then(function(winStatus){
			//update usercampaigns
			if (Array.isArray(winStatus)) winStatus = winStatus[0];
			if (!winStatus) return q.reject(new Error("Can't add winstatus"));
			else{
				code.campaign.winStatus = winStatus;

				return sails.models.usercampaigns.findOrCreate(
					{user:code.user.id,campaign:code.campaign.id},
					{user:code.user.id,campaign:code.campaign.id,winStatus:winStatus.id}
				);
			}
		})
		.then(function(){
			resolve(code);
		})
		.catch(reject);
	});
}

function invalidateWincode(wincode,options){
	sails.log.silly("Invalidate instantwin wincode");
	return q.promise(function(resolve,reject){
		if (!wincode) reject(new Error("invalid wincode"));
		if ((!wincode.campaign) || (!wincode.campaign) || (!wincode.campaign.winStatus)) reject(new Error("invalid populated values"));
		else if (wincode.campaign.winStatus.points<1) reject(errorCodes[1602]) //not enough points
		else{

			wincode.campaign.winStatus.points-=1;
			if ((options) && (options.simulate)) resolve(wincode);
			else sails.models.winstatus.update({id: wincode.campaign.winStatus.id},{points:wincode.campaign.winStatus.points})
				.then(function(winStatuses){
					if (!winStatuses.length) reject(new Error("can't update points"));
					else{
						wincode.campaign.winStatus = winStatuses[0];
						resolve(wincode);
					}
				})
				.catch(reject);
		}

	})
}

function normalizeVideoAction(action){
  if (action){

    var ret = action;

    var containerUp = '<html><head><style>p{text-align: center;}</style><meta name="viewport" content="width=device-width, user-scalable=no,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"></head><body><div style="width:100%;">';
    var containerDown = '</div></body></html>';
    ret = containerUp+ret+containerDown;

    return ret;
  }
}

function getVideoId(action){
  if (action){
    var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    var match = action.match(myregexp);
    if ((match) && (match.length>1)) return match[1];
    else return "";
  }
}