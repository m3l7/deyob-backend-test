//viral campaign -- type 5
var q = require("q");

module.exports = {
	checkCodeValidity: checkCodeValidity,
	scanCode: scanCode,
	invalidateWincode: invalidateWincode,
	updateCampaignInfo: updateCampaignInfo
}

function checkCodeValidity(code,req){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice non presente");
		else if (!code.codeBlock) reject(new Error("Blocco non presente"));
		else q()
		.then(function(){
			if ((code.campaign.winStatus) && (code.campaign.winStatus.codeGenerated==code.id)) reject(errorCodes[1002]);
			else if (code.campaign.winStatus) return sails.models.scanstatus.findOne({
				or:[
					{
						code: code.id,
						user: req.user.id
					},
					{
						code: code.campaign.winStatus.codeGenerated,
						user: req.user.id
					}
				]
			});
		})
		.then(function(scanstatus){
			if (scanstatus) {
				var errorCode = errorCodes[1002];
				if(errorCode.action) delete errorCode.action;
				if(errorCode.actionVideoID) delete errorCode.actionVideoID;
				if(code.codeBlock.action){
					if(code.codeBlock.actionType==1){ //YOUTUBE
							errorCode.actionVideoID = getVideoId(code.codeBlock.actionText);
            				errorCode.action = "<iframe src='https://www.youtube.com/embed/"+errorCode.actionVideoID+"' frameborder='0' allowfullscreen></iframe>";
            				errorCode.action = normalizeVideoAction(errorCode.action);
					}else if(code.codeBlock.actionType==2){ //HTML
							errorCode.action = code.codeBlock.actionText;
					}
				}					
				reject(errorCode);
			} else resolve(code);
		})
		.catch(reject);
	})
}
function scanCode(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice mancante");
		else{
			q()
			.then(function(){
				// generate new winstatus if we are a new user
				if (!code.campaign.winStatus) {
					code.newUser = true;
					return generateChildCodeLog(code);
				}
				else return code;
			})
			.then(function(code){
				//if the user is old, give points to the ~new~ father (-> parentWinstatus), not the original father
				if ((!code.newUser) && (!code.trackingCode)) return [code,sails.models.winstatus.findOne({codeGenerated:code.id})];
				else return [code];
			})
			.spread(function(code,parentWinstatus){
				var parent;
				if (parentWinstatus) parent = parentWinstatus.user;

				//give points and generate virality
				var points = 0;
				if ((!code.newUser) && (!code.trackingCode)) points = 4; //old user, private code logic
				else points = code.codeBlock.points;
				return updateParentsPoints(code.user.id,code.campaign.id,points,0,code.user.id,parent);
			})
			.then(function(obj){
				code.user = obj.user;
				resolve(code);
			})
			.catch(function(err){
				reject(new Error(err));
			})
		}
	})
}

function generateChildCodeLog(code){
	//generate a new winstatus object, and increment number of children of parent
	return q.promise(function(resolve,reject){
		if (!code) reject(new Error("codice non presente"));
		else{
			sails.models.winstatus.findOne({
				codeGenerated: code.id
			})
			.then(function(parentWinstatus){
				return [
					sails.models.code.create({ //generate child code
						encryptedCode: utilsCrypt.uid(32),
						trackingCode: null,
						type: 0,
						points: code.codeBlock.points, //points are inherited
						brand: code.brand,
						campaign: code.campaign,
						product: code.product,
						codeBlock: code.codeBlock.id
					}),
					parentWinstatus
				];
			})
			.spread(function(childCode,parentWinstatus){
				//generate child winstatus
				if (!childCode) reject(new Error("Impossibile generare un nuovo codice"));
				else {
					var promises = [
						childCode,
						sails.models.winstatus.create({
							user: code.user.id,
							campaign: code.campaign.id,
							code: code.id,
							codeGenerated: childCode.id,
							qrcodeShare: childCode.encryptedCode, //link child code with qrcode to be shared
							points: code.user.points,
							parent: (parentWinstatus) ? parentWinstatus.user: null
						})
					];
					if (parentWinstatus) promises.push(sails.models.winstatus.update(parentWinstatus.id,{
						children: parentWinstatus.children + 1
					}));
					return q.all(promises);
				}
			})
			.spread(function(childCode,childWinstatus){
				code.campaign.winStatus = childWinstatus;
				return [
					code,
					sails.models.usercampaigns.create({user:code.user.id,campaign:code.campaign.id,winStatus:childWinstatus})
				];
			})
			.spread(function(code,usercampaign){
				resolve(code);
			})
			.catch(function(err){
				reject(new Error(err));
			});
		}
	})
}

function updateParentsPoints(userId,campaignId,points,position,scanUser, parent){
	//update points of the whole chain, to a max of 4 levels.
	// 0: child
	// 1: father
	// 2: ....

	//parent: override automatic parent detection and set a manual parent
	return q.promise(function(resolve,reject){
		if (!position) position = 0;
		if ((!userId) || (!campaignId) || (!points)) reject(new Error("update chain points: winstatus mancante"));
		else if ((position>3) || (position<0)) resolve();
		else{
			sails.models.user.findOne(userId)
			.then(function(user){
				if (!user) reject(new Error("update chain points: impossibile aggiornare utente"));
				else{
					var pointsToAdd;
					if (position<=1) pointsToAdd = points;
					else if (position==2) pointsToAdd = points*0.5;	
					else pointsToAdd = points*0.25;	
					user.points += pointsToAdd;
					return [
						user.save(),
						user.points,
						pointsToAdd
					]
				}
			})
			.spread(function(user,newPoints,pointsToAdd){
				//update pointslog table
				return [
					user,
					newPoints,
					sails.models.viralpointslog.create({
						user: userId,
						scanUser: scanUser,
						campaign: campaignId,
						points: pointsToAdd,
						social_username: user.social_username,
						first_name: user.first_name,
						last_name: user.last_name
					})
				]
			})
			.spread(function(user,newPoints){
				if (!user) reject(new Error("update chain points: impossibile aggiornare utente"));
				else if (!newPoints) return [
					user,
					sails.models.winstatus.find({
						user: userId,
						campaign: campaignId
					})
				];
				else return [
					user,
					sails.models.winstatus.update({
						user: userId,
						campaign: campaignId
					},{points: newPoints})
				];
			})
			.spread(function(user,winstatuses){
				if (!winstatuses.length) return q.reject(new Error("winstatus mancante"));

				var winstatus = winstatuses[0];
				parent = parent || winstatus.parent;
				if ((position==3) || (!parent)) return [{user:user,winstatus:winstatus}];
				else return [
					{user:user,winstatus:winstatus},
					updateParentsPoints(parent,campaignId,points,position+1,scanUser)
				];
			})
			.spread(function(obj){
				resolve(obj);
			})
			.catch(function(err){
				reject(new Error(err));
			});
		}
	})
}

function invalidateWincode(wincode,options){
	sails.log.silly("Invalidate viral points wincode");
	return q.promise(function(resolve,reject){
		if (!wincode) reject(new Error("invalid wincode"));
		if ((!wincode.campaign) || (!wincode.campaign) || (!wincode.campaign.winStatus)) reject(new Error("invalid populated values"));
		else if (wincode.campaign.winStatus.points<wincode.award.points) reject(errorCodes[1602]) //not enough points
		else{

			wincode.campaign.winStatus.points-=wincode.award.points;
			if ((options) && (options.simulate)) resolve(wincode);
			else sails.models.winstatus.update({id: wincode.campaign.winStatus.id},{points:wincode.campaign.winStatus.points})
				.then(function(winStatuses){
					if (!winStatuses.length) reject(new Error("can't update points"));
					else{
						wincode.campaign.winStatus = winStatuses[0];
						resolve(wincode);
					}
				})
				.catch(reject);
		}

	})
}

function updateCampaignInfo(campaign){
	return q.promise(function(resolve,reject){
		if (!campaign) reject(new Error("campagna non presente"));
		else if ((!campaign.winStatus) || (typeof campaign.winStatus.points!="number")) resolve(campaign);
		else{
			var points = campaign.winStatus.points;

			q()
			.then(function(){
				return [
					sails.models.winstatus.count({
						campaign:campaign.id,
						points:{
							">": points
						}
					}),
					sails.models.winstatus.find({campaign:campaign.id}).sort("points DESC").limit(1)
				];
			})
			.spread(function(position,bestUser){
				campaign.winStatus.position = position+1;
				campaign.winStatus.distance = (bestUser.length) ? bestUser[0].points - points : 0;

				resolve(campaign);
			})
			.catch(reject);
		}
	})
}

function normalizeVideoAction(action){
  if (action){

    var ret = action;

    var containerUp = '<html><head><style>p{text-align: center;}</style><meta name="viewport" content="width=device-width, user-scalable=no,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"></head><body><div style="width:100%;">';
    var containerDown = '</div></body></html>';
    ret = containerUp+ret+containerDown;

    return ret;
  }
}

function getVideoId(action){
  if (action){
    var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    var match = action.match(myregexp);
    if ((match) && (match.length>1)) return match[1];
    else return "";
  }
}
