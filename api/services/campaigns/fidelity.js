//fidelity campaign -- type 4
var q = require("q");

module.exports = {
	checkCodeValidity: checkCodeValidity,
	scanCode: scanCode,
	invalidateWincode: invalidateWincode
}


function checkCodeValidity(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice non presente");
		else if (!code.codeBlock) reject(new Error("Blocco non presente"));
		else if (!code.codeBlock.exported) reject(errorCodes[1003]);
		else sails.models.scanstatus.findOne({code:code.id})
			.then(function(scanstatus){
				if (scanstatus) {
					var errorCode = errorCodes[1002];
					if(errorCode.action) delete errorCode.action;
					if(errorCode.actionVideoID) delete errorCode.actionVideoID;
					if(code.codeBlock.action){
						if(code.codeBlock.actionType==1){ //YOUTUBE
							errorCode.actionVideoID = getVideoId(code.codeBlock.actionText);
            				errorCode.action = "<iframe src='https://www.youtube.com/embed/"+errorCode.actionVideoID+"' frameborder='0' allowfullscreen></iframe>";
            				errorCode.action = normalizeVideoAction(errorCode.action);
						}else if(code.codeBlock.actionType==2){ //HTML
								errorCode.action = code.codeBlock.actionText;
						}
					}					
					reject(errorCode);
				} else resolve(code);
			})
	})
}
function scanCode(code){
	return q.promise(function(resolve,reject){
		if (!code) reject("codice mancante");
		else{
			q().then(function(){
				//update or create winStatus
				return sails.models.winstatus.findOrCreate({
					user: code.user.id,
					campaign: code.campaign.id,
				},
				{
					user: code.user.id,
					campaign: code.campaign.id,
					points: 0
				})	
			})
			.then(function(winstatus){
				if (winstatus) return sails.models.winstatus.update({id:winstatus.id},{points: winstatus.points+1})
			})
			.then(function(winstatus){
				if (winstatus) {
					code.campaign.winStatus = winstatus[0];
					return sails.models.usercampaigns.create({user:code.user.id,campaign:code.campaign.id,winStatus:winstatus.id});
				}
			})
			.then(function(){
				resolve(code);
			})
			.catch(reject);
		}
	})
}
function invalidateWincode(wincode,options){
	sails.log.silly("Invalidate fidelity wincode");
	return q.promise(function(resolve,reject){
		if (!wincode) reject(new Error("invalid wincode"));
		if ((!wincode.campaign) || (!wincode.campaign) || (!wincode.campaign.winStatus)) reject(new Error("invalid populated values"));
		else if (wincode.campaign.winStatus.points<wincode.award.points) reject(errorCodes[1602]) //not enough points
		else{

			wincode.campaign.winStatus.points-=wincode.award.points;
			if ((options) && (options.simulate)) resolve(wincode);
			else sails.models.winstatus.update({id: wincode.campaign.winStatus.id},{points:wincode.campaign.winStatus.points})
				.then(function(winStatuses){
					if (!winStatuses.length) reject(new Error("can't update points"));
					else{
						wincode.campaign.winStatus = winStatuses[0];
						resolve(wincode);
					}
				})
				.catch(reject);
		}

	})
}

function normalizeVideoAction(action){
  if (action){

    var ret = action;

    var containerUp = '<html><head><style>p{text-align: center;}</style><meta name="viewport" content="width=device-width, user-scalable=no,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"></head><body><div style="width:100%;">';
    var containerDown = '</div></body></html>';
    ret = containerUp+ret+containerDown;

    return ret;
  }
}

function getVideoId(action){
  if (action){
    var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    var match = action.match(myregexp);
    if ((match) && (match.length>1)) return match[1];
    else return "";
  }
}