module.exports = function customError(data,options){
	//default options: forceOutput (dev,prod) for non 500.
	//output in dev only mode for 500s

	//TODO: implement options
	var res = this.res;
	if (typeof data == "object"){
		var status = data.status;

		if (status==400) res.badRequest(data,{forceOutput: true});
		else if (status==404) res.notFound();
		else res.serverError(data);
	}
	else res.serverError(new Error(data));
}