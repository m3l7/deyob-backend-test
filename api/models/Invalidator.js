/**
* Invalidator.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
  	user:{
  		model: "user",
  		required: true
  	},
  	brand:{
  		model: "user",
  		required: true
  	},

  	//populated values
  	user_social_username: "string",
  	user_email: "string"
  }
};

