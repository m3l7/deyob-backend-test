/**
* Product.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	name:{
  		type: "string",
  		required: true
  	},
  	description:{
  		type: "string",
      size: 512
  	},
  	expire:{
  		type:"datetime",
  	},
  	images:{
  		collection: "imageproduct",
      via: "product"
  	},
    brand:{
      model: "user"
    },
    link: {
      type: "string",
      defaultsTo: ""
    },
    toJSON: function(){
      this.sanitize();
      return this;
    },
    sanitize: function(){
      if ((!!this.images) && (this.images.length)) this.images.forEach(function(image){
          image.sanitize();
      });

      if ((this.link) && (this.link.indexOf("http")==-1)) this.link = "http://"+this.link;

      return this;
    }
  }
};

