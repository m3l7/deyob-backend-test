/**
* Codeblock.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

//TYPE (VIRAL ONLY)
// 1, MOTHER CODE
// 2: SPOT CODE

module.exports = {

  attributes: {
  	begin:{
  		type: "integer",
      size: 64,
  		required: true
  	},
  	end:{
  		type: "integer",
      size: 64,
  		required: true
  	},
  	emptyAfter:{
  		type: "integer",
      size: 64,
  		required: true
  	},
  	emptyBefore:{
  		type: "integer",
      size: 64,
  		required: true
  	},
  	next:{
  		type:"integer",
      size: 64,
  		required: true
  	},
  	prev:{
  		type:"integer",
      size: 64,
  		required: true
  	},
    date:{
      type: "datetime",
      required: true
    },
  	description:{
  		type: "string",
  		required: false,
      defaultsTo: ""
  	},

    //ACTION
    	action:{
    		type: "boolean",
    		required: true
    	},
      actionType: {
        type: "integer",
        defaultsTo: 0
      },
      actionText: {
        type: "string",
        size: 512
      },
      actionImage: {
        type: "string",
        size: 512
      },


  	exported:{
  		type: "boolean",
  		required: true
  	},
    deleted:{
      type: 'boolean',
      defaultsTo: false
    },
    assigned:{
      type: "boolean",
      defaultsTo: false
    },
    points:{
      type: "integer",
      defaultsTo: 0
    },
    type: {
      type: "integer",
      size: 8,
      defaultsTo: 0
    },
    winners:{
      type: "integer",
      defaultsTo: 0
    },

    //ASSOCIATIONS
    brand:{
      model:'user'
    },
    campaign:{
      model: 'campaign'
    },
    product:{
      model:"product"
    },

    toJSON: function(){
      this.quantity = this.end - this.begin + 1;
      // this.begin = utilsCode.toCode(this.begin);
      // this.end = utilsCode.toCode(this.end);
      // this.prev = utilsCode.toCode(this.prev);
      // this.next = utilsCode.toCode(this.next);

      if ((this.actionImage) && (this.actionImage.indexOf(sails.config.deyob.API.store)==-1)) this.actionImage = sails.config.deyob.API.store + this.actionImage;

      return this;
    }


  }
};

