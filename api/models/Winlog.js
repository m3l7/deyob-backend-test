/**
* Winlog.js
*
* @description :: Model for logging of winning (invalidation) of an award
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
		//ASSOCIATIONS
		user:{
			model: 'user',
			required: true
		},
		brand:{
			model:'user',
			required: true
		},
		campaign:{
			model: 'campaign',
			required: true
		},
		product:{
			model:"product"
		},
		code:{
			model: "wincode",
			required: true
		},
		award:{
			model: "award",
			required: true
		},

		//GEOCODING
		location:{
		  type: 'string',
		  defaultsTo: ''
		},
		country:{
		    type:'string',
		    defaultsTo: ''
		  },
		countryCode:{
		  type: 'string',
		  defaultsTo: ''
		},
		street:{
		  type:"string",
		  defaultsTo: ""
		},
		os:{
		  type: 'string',
		  defaultsTo: ''
		},
		scanDate:{
		  type: 'datetime',
		},
		lng:{
		  type: 'string',
		  defaultsTo: ''
		},
		lat:{
		  type: 'string',
		  defaultsTo: ''
		}
  }

};

