/**
* Scanstatus.js
*
* @description :: Log code scans.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	  //ASSOCIATIONS
  		user:{
  			model: 'user'
  		},
		brand:{
			model:'user'
		},
		campaign:{
			model: 'campaign'
		},
		product:{
			model:"product"
		},
		code:{
			model: "code",
			required: true
		}
  }
};

