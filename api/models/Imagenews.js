/**
* Imagenews.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	path:{
  		type: 'string',
  		defaultsTo: ''
  	},
  	news:{
  		model: 'news'
  	},
    toJSON: function(){
      this.sanitize();
      return this;
    },
  	sanitize: function(){
  		if ((!!this.path) && (this.path.indexOf('http')==-1)) this.path = sails.config.deyob.API.store + this.path;
      if ((!!this.news) && (typeof this.news == 'object')) this.news.sanitize();
  		return this;
  	}
  }
};

