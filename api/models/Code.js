/**
* Code.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var mustache = require("mustache");
var fs = require("fs");

module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
  	trackingCode:{
      type: 'integer',
      size: 64,
      unique: true
    },
  	clearCode:{
      type: 'string',
      defaultsTo: ''
    },
  	encryptedCode:{
      type: 'string',
      defaultsTo: ''
    },
  	cipherPass:{
      type: 'string',
      defaultsTo: ''
    },
    winner:{
      type: 'boolean',
      defaultsTo: false
    },
    codeBlock:{
      model: "codeBlock",
      required: true
    },

    toJSON:function(){
      this.sanitize();
      return this;
    },
  	sanitize: function(){
  		delete this.cipherPass;
  		delete this.clearCode;
  		delete this.cipherPass;

      //normalize trackingCode
      if (this.trackingCode){
        this.trackingCode = this.trackingCode.toString(16);
        while (this.trackingCode.length!=11) this.trackingCode = '0'+this.trackingCode;
      }

      if (this.codeBlock){
        //fill data from codeBlock
        this.codeBlock = this.codeBlock.toJSON();

        //build action
        if (this.codeBlock.action){
          if (this.codeBlock.actionType==2) {
            this.action = fs.readFileSync("./views/action.min.html").toString();
            this.action = this.action.replace("{{actionImage}}",this.codeBlock.actionImage);
            this.action = this.action.replace("{{actionText}}",this.codeBlock.actionText);
          }
          else if (this.codeBlock.actionType==1){
            this.actionVideoID = getVideoId(this.codeBlock.actionText);
            this.action = "<iframe src='https://www.youtube.com/embed/"+this.actionVideoID+"' frameborder='0' allowfullscreen></iframe>";
            this.action = normalizeVideoAction(this.action);
          }
        }
        this.points = this.codeBlock.points;
        delete this.codeBlock;
      }

      //normalize winning qrcode
      if ((this.brand) && (typeof this.brand == 'object')) this.brand.sanitize();
      if ((this.user) && (typeof this.user == 'object')) this.user.sanitize();
      if ((this.campaign) && (typeof this.campaign == 'object')) this.campaign.sanitize();
      
  		return this;
  	}
  }
};

function normalizeVideoAction(action){
  if (action){

    var ret = action;

    var containerUp = '<html><head><style>p{text-align: center;}</style><meta name="viewport" content="width=device-width, user-scalable=no,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"></head><body><div style="width:100%;">';
    var containerDown = '</div></body></html>';
    ret = containerUp+ret+containerDown;

    return ret;
  }
}

function getVideoId(action){
  if (action){
    var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    var match = action.match(myregexp);
    if ((match) && (match.length>1)) return match[1];
    else return "";
  }
}
