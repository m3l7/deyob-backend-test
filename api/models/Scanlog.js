/**
* Scanlog.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	  newUser:{
  	    type: 'boolean',
  	    defaultsTo: true
  	  },
  	  lng:{
  	    type: 'string',
  	    defaultsTo: ''
  	  },
  	  lat:{
  	    type: 'string',
  	    defaultsTo: ''
  	  },

  	  //GEOCODING
  	  location:{
  	    type: 'string',
  	    defaultsTo: ''
  	  },
  	  country:{
  	      type:'string',
  	      defaultsTo: ''
  	    },
  	  countryCode:{
  	    type: 'string',
  	    defaultsTo: ''
  	  },
  	  street:{
  	    type:"string",
  	    defaultsTo: ""
  	  },
  	  os:{
  	    type: 'string',
  	    defaultsTo: ''
  	  },
  	  scanDate:{
  	    type: 'datetime',
  	  },

  	  //ASSOCIATIONS
  		user:{
  			model: 'user'
  		},
		  brand:{
		    model:'user'
		  },
		  campaign:{
		    model: 'campaign'
		  },
		  product:{
		    model:"product"
		  },
		  code:{
		  	model: "code",
		  	required: true
		  },
      codeblock:{
        model: "codeblock",
        required: true
      },


      // USER POPULATE
      user_email:{
        type:'email_or_empty'
      },
      user_username: {
        type: "string",
        required:'true'
      },
      user_first_name:{
        type: "string",
        defaultsTo: ''
      },
      user_last_name:{
        type: "string",
        defaultsTo: ''
      },
      user_age:{
        type: "integer",
        defaultsTo: 0
      },
      user_gender:{
        type: "string",
        defaultsTo: ''
      },
      user_image:{
        type: "string",
        defaultsTo: ''
      },

  }
};

