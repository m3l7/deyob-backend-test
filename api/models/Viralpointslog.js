/**
* Viralpointslog.js
*
* @description :: points log for viral campaigns. Used in viral campaign ranks API
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	user:{ //user who received the points
  		model:'user',
  	},
  	scanUser:{ //user who received the points
  		model:'user',
  	},
  	campaign:{
  		model:'campaign'
  	},
  	points:{
  		type: "integer",
  		required: true,
  		defaultsTo: 0
  	},
  	social_username: "string",
  	first_name: "string",
  	last_name: "string"
  }
};

