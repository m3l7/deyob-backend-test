/**
* Award.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	name:{
      type:'string',
      defaultsTo: ''
    },
  	description:{
      type: 'string',
      defaultsTo: '',
      size: 512
    },
  	winnersNumber: {
      type: 'integer',
      defaultsTo: 0
    },
  	points:{
      type: 'integer',
      defaultsTo: 0
    },
  	image:{
      type: 'string',
      defaultsTo: ''
    },
  	campaign:{
  		model:'campaign'
  	},
    toJSON: function(){
      this.sanitize();
      return this;
    },
    sanitize: function(){
      if ((this.image) && (this.image.indexOf('http')==-1)) this.image = sails.config.deyob.API.store + this.image;
      if ((this.qrcode) && (this.qrcode.indexOf(sails.config.deyob.API.qrcodeInvalidate)==-1)) this.qrcode = sails.config.deyob.API.qrcodeInvalidate + this.qrcode;
      return this;
    }
  }
};

