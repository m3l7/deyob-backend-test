/**
* Winstatus.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

// NB: for VIRAL codes, "code" is the ~parent~ code, ~qrcode~ is the new own generated code

module.exports = {

  attributes: {
  	user:{
  		model:"user",
  		required:true
  	},
  	campaign:{
  		model:"campaign",
  		required:true
  	},
  	code:{
  		model:"code"
  	},
    codeGenerated:{
      model: "code"
    },
  	qrcodeShare:{
  		type:"string"
  	},
  	points: {
      type: "float",
      defaultsTo: 0
    },
    parent:{
      model: "user"
    },
    children:{
      type: "integer",
      defaultsTo: 0
    },
    toJSON: function(){
      this.sanitize();
      return this;
    },
    sanitize: function(){
      if (!this.qrcode) delete qrcode;
      else if (this.qrcode.indexOf(sails.config.deyob.API.qrcodeInvalidate)==-1) this.qrcode = sails.config.deyob.API.qrcodeInvalidate + this.qrcode;
      if (!this.qrcodeShare) delete qrcodeShare;
      else if (this.qrcodeShare.indexOf(sails.config.deyob.API.qrcodeScan)==-1) this.qrcodeShare = sails.config.deyob.API.qrcodeScan + this.qrcodeShare;

      delete this.codeGenerated;
      delete this.parent;
      return this;
    }
  }
};

