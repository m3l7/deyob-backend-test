/**
* Campaign.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


// TYPES:
//   1: lotteria
//   2: instant win
//   3: raccolta punti
//   4: acquisto prodotto
//   5: viral

module.exports = {

  attributes: {
    type:{
      type: 'integer',
      defaultsTo: 0
    },
    typeName:{
      type: 'string',
      defaultsTo: ''
    },
    dateStart:{
      type: 'date',
    },
    dateEnd:{
      type: 'date',
    },
    description:{
      type: 'string',
      defaultsTo: '',
      size: 512
    },
    image:{
      type: "string",
      defaultsTo: ''
    },
    link:{
      type: 'string',
      defaultsTo: ''
    },
    email:{
      type:'email',
      defaultsTo: ''
    },
    name:{
      type: "string",
      defaultsTo: '',
      required: true
    },
    awards:{
      collection:'award',
      via:'campaign'
    },  
  	brand:{
  		model: 'user',
  		required: true
  	},
    uniqueUsers:{
      type: "integer",
      defaultsTo: 0
    },
    visible:{
      type: "boolean",
      defaultsTo: true
    },
    toJSON: function(){
      this.sanitize();
      return this;
    },
    sanitize: function(){
      if ((this.image) && (this.image.indexOf('http')==-1)) this.image = sails.config.deyob.API.store + this.image;
      if ((this.brand) && (typeof this.brand == 'object') && (this.brand.sanitize)) this.brand.sanitize();
      if ((this.awards) && (this.awards.length)) this.awards.forEach(function(award){
        if (award.sanitize) award.sanitize();
      });
      if ((this.winStatus) && (this.winStatus.sanitize)) this.winStatus.sanitize();

      if ((this.link) && (this.link.indexOf("http")==-1)) this.link = "http://"+this.link;

      //check if campaign is expired
      utilsCampaign.checkExpired(this);

      return this;
    }
  },
  afterCreate: function(record,cb){
    //send push notification
    utilsPush.sendPushToChannel("Nuova campagna: "+record.name,["deyob_campagne"], "campaign")
        .catch(function(err){
          sails.log.warn("Can't send push for campaign: "+err);
        })


    cb();
  }
};
