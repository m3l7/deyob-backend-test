/**
* Wincode.js
*
* @description :: store a qrcode for each award-user couple
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	campaign:{
  		model: "campaign",
  		required: true
  	},
  	award:{
  		model: "award",
  		required: true
  	},
  	user:{
  		model: "user",
  		required: true
  	},
  	brand:{
  		model: "user",
  		required: true
  	},
  	qrcode:{
  		type: "string",
  		required: true
  	}
  }
};

