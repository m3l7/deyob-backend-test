/**
* News.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	title: {
  		type:"string",
  		required: true
  	},
  	description:{
  		type:"string",
  		defaultsTo: '',
      size: 2000
  	},
    link:{
      type:"string",
      defaultsTo: ''
    },
  	images:{
  		collection: 'imagenews',
  		via: 'news'
  	},
    toJSON: function(){
      this.sanitize();
      return this;
    },
  	sanitize: function(){
  		if ((!!this.images) && (this.images.length)) this.images.forEach(function(image){
  				image.sanitize();
  		});
      return this;
  	}
  },
  afterCreate: function(record,cb){
    //send push notification
    utilsPush.sendPushToChannel(record.title,["deyob_news"],"news")
      .catch(function(err){
        sails.log.warn("Can't send push for news: "+err);
      });

    cb();
  }
};

